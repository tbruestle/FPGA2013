-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II"
-- VERSION		"Version 10.1 Build 197 01/19/2011 Service Pack 1 SJ Full Version"
-- CREATED		"Sat Feb 09 23:34:00 2013"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY two_bit_mux IS 
	PORT
	(
		in_0 :  IN  STD_LOGIC;
		in_1 :  IN  STD_LOGIC;
		sel :  IN  STD_LOGIC;
		out_0 :  OUT  STD_LOGIC
	);
END two_bit_mux;

ARCHITECTURE bdf_type OF two_bit_mux IS 

SIGNAL	sel_1 :  STD_LOGIC;
SIGNAL	sel_0 :  STD_LOGIC;
SIGNAL	notsel :  STD_LOGIC;

COMPONENT AND_gate
	PORT(x: in std_logic;
	y: in std_logic;
	F: out std_logic
	);
END COMPONENT;

COMPONENT OR_gate
	PORT(x: in std_logic;
	y: in std_logic;
	F: out std_logic
	);
END COMPONENT;

COMPONENT INV_gate 
PORT(	x: in std_logic;
		F: out std_logic
);
END COMPONENT;

BEGIN 


not1 : INV_gate
PORT MAP(x => sel,
		 F => notsel);
		 

and1 : AND_gate
PORT MAP(x => sel,
		 y => in_1,
		 F => sel_1);
		 
and2 : AND_gate
PORT MAP(x => notsel,
		 y => in_0,
		 F => sel_0);
		 
or1 : OR_gate
PORT MAP(x => sel_1,
		 y => sel_0,
		 F => out_0);


END bdf_type;