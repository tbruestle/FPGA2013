
# Step 1 - 5 can also be done by using menu items

# 1) Create a library for working in, 
# vlib work

# 2) Compile the half adder
# vcom -93 -explicit -work work counter.vhd

# 3) Load it for simulation
# vsim counter

# 4) Open some selected windows for viewing
# view structure
# view signals
# view wave

# 5) Show some of the signals in the wave window
# command add wave ?r * open the wave windows with all the signals listed

# add wave /count 
# add wave /clk 
# add wave /reset  

add wave -r * 

# 6) Set some test patterns

# clock = 0 at 0 ns, 1 at 10 ns, and repeat the pattern every 20 ns
force clock_50mhz 0 0 ns, 1 10 ns -r 20 ns

# reset = 1 at 0 ns, 0 after 20 ns
force reset 1 0 ns , 0 20 ns -r 400 ns


force add_sub 0 0 ns, 1 300 ns -r 500 ns
force enable 1 0 ns,  0 100 ns -r 700 ns

# count = 00000000 at 0, count from 0
# force count 2#00000000 0 ns     


# 7) Run the simulation for 40 ns
run 1000 ns



