-------------------------------------------
-- Dataflow models of basic logic gates: --
-- AND, NAND, OR, NOR, XOR, XNOR, INV    --
-- filename: basicgates.vhd              --
-------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

--------------------------------------------------

entity AND_gate is
port(	x: in std_logic;
	y: in std_logic;
	F: out std_logic
);
end AND_gate;  

--------------------------------------------------
architecture behav of AND_gate is
begin

    F <= x and y;

end behav;

--------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity NAND_gate is
port(	x: in std_logic;
	y: in std_logic;
	F: out std_logic
);
end NAND_gate;  

-----------------------------------------

architecture behav of NAND_gate is 
begin 

	F <= x nand y; 

end behav;


--------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity OR_gate is
port(	x: in std_logic;
	y: in std_logic;
	F: out std_logic
);
end OR_gate;  

---------------------------------------

architecture behav of OR_gate is 
begin 

    F <= x or y; 

end behav;

-----------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity NOR_gate is
port(	x: in std_logic;
	y: in std_logic;
	F: out std_logic
);
end NOR_gate;  

------------------------------------------
architecture behav of NOR_gate is 
begin 

    F <= x nor y; 

end behav;

---------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity XOR_gate is
port(	x: in std_logic;
	y: in std_logic;
	F: out std_logic
);
end XOR_gate;  

--------------------------------------

architecture behav of XOR_gate is 
begin 

    F <= x xor y; 

end behav;

--------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity XNOR_gate is
port(	x: in std_logic;
	y: in std_logic;
	F: out std_logic
);
end XNOR_gate;  

--------------------------------------

architecture behav of XNOR_gate is 
begin 

    F <= x xnor y; 

end behav;

---------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity INV_gate is
port(	x: in std_logic;
		F: out std_logic
);
end INV_gate;  

--------------------------------------

architecture behav of INV_gate is 
begin 

    F <= not x; 

end behav;

---------------------------------------
