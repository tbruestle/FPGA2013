-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II"
-- VERSION		"Version 10.1 Build 197 01/19/2011 Service Pack 1 SJ Full Version"
-- CREATED		"Sat Feb 09 23:32:54 2013"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY four_bit_iterator IS 
	PORT
	(
		reset :  IN  STD_LOGIC;
		enable :  IN  STD_LOGIC;
		clk :  IN  STD_LOGIC;
		add_sub :  IN  STD_LOGIC;
		Sum0 :  OUT  STD_LOGIC;
		Sum1 :  OUT  STD_LOGIC;
		Sum2 :  OUT  STD_LOGIC;
		Sum3 :  OUT  STD_LOGIC
	);
END four_bit_iterator;

ARCHITECTURE bdf_type OF four_bit_iterator IS 

COMPONENT add_dff
	PORT(reset : IN STD_LOGIC;
		 enable : IN STD_LOGIC;
		 add_sub : IN STD_LOGIC;
		 add_toggle : IN STD_LOGIC;
		 sub_toggle : IN STD_LOGIC;
		 clk : IN STD_LOGIC;
		 sum : OUT STD_LOGIC
	);
END COMPONENT;

COMPONENT AND_gate
	PORT(x: in std_logic;
	y: in std_logic;
	F: out std_logic
	);
END COMPONENT;

COMPONENT OR_gate
	PORT(x: in std_logic;
	y: in std_logic;
	F: out std_logic
	);
END COMPONENT;

COMPONENT INV_gate 
PORT(	x: in std_logic;
		F: out std_logic
);
END COMPONENT;

SIGNAL	temp_sum_0 :  STD_LOGIC;
SIGNAL	not_temp_sum_0 :  STD_LOGIC;
SIGNAL	temp_sum_1 :  STD_LOGIC;
SIGNAL	temp_sum_2 :  STD_LOGIC;
SIGNAL	temp_sum_1_and_0 :  STD_LOGIC;
SIGNAL	temp_sum_1_or_0 :  STD_LOGIC;
SIGNAL	temp_sum_1_nor_0 :  STD_LOGIC;
SIGNAL	temp_sum_2_1_and_0 :  STD_LOGIC;
SIGNAL	temp_sum_2_1_or_0 :  STD_LOGIC;
SIGNAL	temp_sum_2_1_nor_0 :  STD_LOGIC;


BEGIN 
Sum0 <= temp_sum_0;
Sum1 <= temp_sum_1;
Sum2 <= temp_sum_2;



b2v_inst : add_dff
PORT MAP(reset => reset,
		 enable => enable,
		 add_sub => add_sub,
		 add_toggle => '1',
		 sub_toggle => '1',
		 clk => clk,
		 sum => temp_sum_0);


b2v_inst1 : add_dff
PORT MAP(reset => reset,
		 enable => enable,
		 add_sub => add_sub,
		 add_toggle => temp_sum_0,
		 sub_toggle => not_temp_sum_0,
		 clk => clk,
		 sum => temp_sum_1);


and1_0 : AND_gate
PORT MAP(x => temp_sum_0,
		 y => temp_sum_1,
		 F => temp_sum_1_and_0);
		 

and2_1_0 : AND_gate
PORT MAP(x => temp_sum_1_and_0,
		 y => temp_sum_2,
		 F => temp_sum_2_1_and_0);
		 
or1_0 : OR_gate
PORT MAP(x => temp_sum_0,
		 y => temp_sum_1,
		 F => temp_sum_1_or_0);
		 
or2_1_0 : OR_gate
PORT MAP(x => temp_sum_1_or_0,
		 y => temp_sum_2,
		 F => temp_sum_2_1_or_0);



inv1_0 : INV_gate
PORT MAP(x => temp_sum_1_or_0,
		 F => temp_sum_1_nor_0);
		 


inv2_1_0 : INV_gate
PORT MAP(x => temp_sum_2_1_or_0,
		 F => temp_sum_2_1_nor_0);


b2v_inst2 : add_dff
PORT MAP(reset => reset,
		 enable => enable,
		 add_sub => add_sub,
		 add_toggle => temp_sum_1_and_0,
		 sub_toggle => temp_sum_1_nor_0,
		 clk => clk,
		 sum => temp_sum_2);


b2v_inst3 : add_dff
PORT MAP(reset => reset,
		 enable => enable,
		 add_sub => add_sub,
		 add_toggle => temp_sum_2_1_and_0,
		 sub_toggle => temp_sum_2_1_nor_0,
		 clk => clk,
		 sum => Sum3);


inv_0 : INV_gate
PORT MAP(x => temp_sum_0,
		 F => not_temp_sum_0);



END bdf_type;