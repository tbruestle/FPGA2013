-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II"
-- VERSION		"Version 10.1 Build 197 01/19/2011 Service Pack 1 SJ Full Version"
-- CREATED		"Sat Feb 09 23:31:10 2013"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY add_dff IS 
	PORT
	(
		reset :  IN  STD_LOGIC;
		enable :  IN  STD_LOGIC;
		clk :  IN  STD_LOGIC;
		add_sub :  IN  STD_LOGIC;
		add_toggle :  IN  STD_LOGIC;
		sub_toggle :  IN  STD_LOGIC;
		sum :  OUT  STD_LOGIC
	);
END add_dff;

ARCHITECTURE bdf_type OF add_dff IS 

COMPONENT two_bit_mux
	PORT(sel : IN STD_LOGIC;
		 in_0 : IN STD_LOGIC;
		 in_1 : IN STD_LOGIC;
		 out_0 : OUT STD_LOGIC
	);
END COMPONENT;

COMPONENT d_ff 
PORT ( 
	d,clk,reset,set,enable : in STD_LOGIC; 
	q  : out STD_LOGIC
	); 
END COMPONENT;

COMPONENT AND_gate
	PORT(x: in std_logic;
	y: in std_logic;
	F: out std_logic
	);
END COMPONENT;

COMPONENT OR_gate
	PORT(x: in std_logic;
	y: in std_logic;
	F: out std_logic
	);
END COMPONENT;

COMPONENT INV_gate 
PORT(	x: in std_logic;
		F: out std_logic
);
END COMPONENT;


SIGNAL	toggled_sum :  STD_LOGIC;
SIGNAL	do_toggle :  STD_LOGIC;
SIGNAL	temp_sum :  STD_LOGIC;
SIGNAL	not_temp_sum :  STD_LOGIC;
SIGNAL	new_sum :  STD_LOGIC;
SIGNAL	toggle_bit :  STD_LOGIC;


BEGIN 
sum <= temp_sum;


b2v_inst : two_bit_mux
PORT MAP(sel => add_sub,
		 in_0 => add_toggle,
		 in_1 => sub_toggle,
		 out_0 => toggle_bit);


b2v_inst13 : two_bit_mux
PORT MAP(sel => reset,
		 in_0 => toggled_sum,
		 in_1 => '0',
		 out_0 => new_sum);



b2v_inst15 : two_bit_mux
PORT MAP(sel => do_toggle,
		 in_0 => temp_sum,
		 in_1 => not_temp_sum,
		 out_0 => toggled_sum);

d_ff1 : d_ff
PORT MAP(
	d => new_sum,
	clk => clk,
	reset => '0',
	set => '0',
	enable => '1',
	q => temp_sum);
		 
and1 : AND_gate
PORT MAP(x => enable,
		 y => toggle_bit,
		 F => do_toggle);
		 
not1 : INV_gate
PORT MAP(x => temp_sum,
		 F => not_temp_sum);



END bdf_type;