LIBRARY ieee; 
USE ieee.std_logic_1164.all; 

entity d_ff is 
port ( 
	d,clk,reset,set,enable : in STD_LOGIC; 
	q  : out STD_LOGIC
	); 
end d_ff; 

architecture beh of d_ff is 
begin 
	process(clk, enable) 
	begin 
		if clk'event and clk='1' and enable='1' then 
			if reset='1' then 
				 q <= '0'; 
			elsif set='1' then 
				 q <= '1'; 
			else 
				 q <= d; 
			end if; 
		end if; 
	end process; 
end beh;