--Thomas Bruestle
--Greg Sleasman
--
--Vga_word_parse.vhd 
--
--This turns takes the 8 character string of VGA and calls Vga_letter_parse.vhd for each letter.


  library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.STD_LOGIC_ARITH.ALL;
  use IEEE.STD_LOGIC_UNSIGNED.ALL;
  use IEEE.vga_package.all;
 
  entity vga_word_parse is
    port(letters  : in EIGHT_STRING;
	      blocks  : out LETTER_BLOCK_ARRAY);
  end vga_word_parse;
  
  
ARCHITECTURE arch_vga_word_parse OF vga_word_parse IS

	COMPONENT  vga_letter_parse
	PORT
	(
		letter  : in character;
	   blockin  : out LETTER_BLOCK
	);
	END COMPONENT;
BEGIN

   GEN_vga_letter_parse: 
   for I in letter_num_min to letter_num_max  generate
      vga_letter_parseX : vga_letter_parse port map
			(	letter => letters(I), blockin => blocks(I) );
   end generate GEN_vga_letter_parse;
END arch_vga_word_parse;