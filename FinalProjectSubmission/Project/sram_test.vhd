--Thomas Bruestle
--Greg Sleasman
--
-- Sram _test.vhd (Top level entity) 
--
--This is an alternate implementation of homework 5 using the SRAM.
--This is a simple implementation of reading from and writing to SRAM.


LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;
USE  IEEE.sram_package.all;

entity sram_test is
	port(
		CLOCK_50  : in std_logic;
		SW: in std_logic_vector(17 downto 0);
		KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		--LEDR: out std_logic_vector(17 downto 0);
		
		
		-- Representative Display
		
		LEDG: out std_logic_vector(7 downto 0); --WE
		
		
		HEX7: out bit_vector(0 to 6); --address
		HEX6: out bit_vector(0 to 6); --address
		
		HEX5: out bit_vector(0 to 6); --memory input
		HEX4: out bit_vector(0 to 6); --memory input
		
		HEX1: out bit_vector(0 to 6); --memory output
		HEX0: out bit_vector(0 to 6); --memory output
		
		
		-- SRAM
		
		SRAM_ADDR: out std_logic_vector(19 downto 0);
		SRAM_DQ: inout std_logic_vector(15 downto 0);
		SRAM_CE_N: out std_logic;
		SRAM_OE_N: out std_logic;
		SRAM_WE_N: out std_logic;
		SRAM_UB_N: out std_logic;
		SRAM_LB_N: out std_logic
	);
end sram_test;


architecture arch_sram_test of sram_test is

--	signal address: std_logic_vector(4 downto 0);
--	
--	signal input_data: std_logic_vector(7 downto 0);
--	
--	signal output_data: std_logic_vector(7 downto 0);
--	signal we: std_logic;
		
	
	signal new_input_data: sram_data_type_array;
 	signal new_output_data: sram_data_type_array;
	signal new_address: sram_address_type_array;
	signal new_write: sram_write_array;
	signal new_enable: sram_enable_array;
	
	
	COMPONENT seven_led
	PORT(	
		vis_in:	in bit;
		bin_in:	in bit_vector (3 downto 0);
		hex_out:	out bit_vector (0 to 6)
		);
	END COMPONENT; 
	
	COMPONENT sram
	PORT(
		CLOCK_50  : in std_logic;
		new_write: in sram_write_array;
		new_address: in sram_address_type_array;
		new_input_data: in sram_data_type_array;
		new_enable: IN sram_enable_array;
		new_top_enable: IN sram_enable_array;
		new_bottom_enable: IN sram_enable_array;
		new_output_data: OUT sram_data_type_array;
		
		
		-- SRAM
		
		SRAM_ADDR: out std_logic_vector(19 downto 0);
		SRAM_DQ: inout std_logic_vector(15 downto 0);
		SRAM_CE_N: out std_logic;
		SRAM_OE_N: out std_logic;
		SRAM_WE_N: out std_logic;
		SRAM_UB_N: out std_logic;
		SRAM_LB_N: out std_logic
	);
	END COMPONENT; 


begin

	new_input_data(0)(15 downto 8) <= (others => '0');
	new_input_data(0)(7 downto 0) <= SW(7 downto 0);
	
	new_address(0)(15 downto 5) <= (others => '0');
	new_address(0)(4 downto 0) <= SW(15 downto 11);

	new_write(0) <= SW(17);
	
	new_enable(0) <= not KEY(0);
	

sram1: sram	PORT MAP(
	CLOCK_50 => CLOCK_50,
	new_write => new_write,
	new_address => new_address,
	new_input_data => new_input_data,
	new_enable => new_enable,
	new_top_enable => new_enable,
	new_bottom_enable => new_enable,
	new_output_data => new_output_data,
	SRAM_ADDR => SRAM_ADDR,
	SRAM_DQ => SRAM_DQ,
	SRAM_CE_N => SRAM_CE_N,
	SRAM_OE_N => SRAM_OE_N,
	SRAM_WE_N => SRAM_WE_N,
	SRAM_UB_N => SRAM_UB_N,
	SRAM_LB_N => SRAM_LB_N
 ); --address

	LEDG(0) <= new_write(0); --write enable
	LEDG(1) <= new_enable(0); --write enable
	
	sled7: seven_led	PORT MAP( vis_in => '1', bin_in => to_bitvector("000" &  new_address(0)(4)), hex_out(0 to 6) => HEX7(0 to 6) ); --address
	sled6: seven_led	PORT MAP( vis_in => '1', bin_in => to_bitvector(new_address(0)(3 downto 0)), hex_out(0 to 6) => HEX6(0 to 6) ); --address
	
	sled5: seven_led	PORT MAP( vis_in => to_bit(new_write(0)) and to_bit( new_enable(0)), bin_in => to_bitvector(new_input_data(0)(7 downto 4)), hex_out(0 to 6) => HEX5(0 to 6) ); --memory input
	sled4: seven_led	PORT MAP( vis_in => to_bit(new_write(0)) and to_bit( new_enable(0)), bin_in => to_bitvector(new_input_data(0)(3 downto 0)), hex_out(0 to 6) => HEX4(0 to 6) ); --memory input
	
	sled1: seven_led	PORT MAP( vis_in => to_bit(not new_write(0)) and to_bit( new_enable(0)), bin_in => to_bitvector(new_output_data(0)(7 downto 4)), hex_out(0 to 6) => HEX1(0 to 6) ); --memory output
	sled0: seven_led	PORT MAP( vis_in => to_bit(not new_write(0)) and to_bit( new_enable(0)), bin_in => to_bitvector(new_output_data(0)(3 downto 0)), hex_out(0 to 6) => HEX0(0 to 6) ); --memory output
	




end arch_sram_test;