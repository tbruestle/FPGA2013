vcom -93 -explicit -work work cmd_decode.vhd
vsim cmd_decode
    
force iCLK 0 0 ns, 1 10 ns -repeat 20 ns
force iRST_n 0 0 ns, 1 20 ns

force iRXD_DATA 00000000 0 ns, 10000011 20 ns, 11110000 40 ns, 00000011 60 ns, 00110100 80 ns, 01010110 100 ns, 00000001 120 ns, 11011001 140 ns, 11001100 160 ns
                ## reset       ## CMD_Action = x83 (WRITE)     ## CMD_ADDR(23 downto 0) = 000000 + oLED_RED     ## CMD_DATA(15 downto 0) = 0000000 + oLED_GREEN
                                               ## CMD_Target	= xF0 (LED)                                                                          ## CMD_MODE	= xCC (DISPLAY)
 
force iRXD_Ready 0 0 ns, 1 20 ns, 0 180 ns


add wave /iCLK
add wave /iRST_n
add wave /iRXD_DATA
add wave /iRXD_Ready


add wave /CMD_Tmp


add wave /CMD_Action	
add wave /CMD_Target	
add wave /CMD_ADDR	
add wave /CMD_DATA	
add wave /CMD_MODE		

add wave /Pre_Target	
	
add wave /f_LED

add wave /oLED_RED
add wave /oLED_GREEN


run 200 ns