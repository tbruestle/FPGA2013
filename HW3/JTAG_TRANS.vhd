library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

Entity JTAG_TRANS is
port(	
--	HOST	
	iTxD_DATA : in std_logic_vector(7 downto 0);
	iTxD_Start : in std_logic;
	oTxD_Done : out std_logic;
--	JTAG
	TDO : out std_logic;
	TCK : in std_logic;
	TCS : in std_logic
);
end JTAG_TRANS;
			
architecture jt of JTAG_TRANS is
	signal rCont  : std_logic_vector(2 downto 0);
begin
	process(TCK, TCS) 
	begin
		 --if (TCK'event and TCK='1') or (TCS'event and TCS='1') then 
			if (TCS='1') then
				oTxD_Done<='0';
				rCont<=O"0";
				TDO<='0';
			elsif (TCK'event and TCK='1') then
				if(iTxD_Start = '1') then
					rCont <= rCont+1;
					if rCont = x"0" then
						TDO <= iTxD_DATA(0);
					elsif rCont = x"1" then
						TDO <= iTxD_DATA(1);
					elsif rCont = x"2" then
						TDO <= iTxD_DATA(2);
					elsif rCont = x"3" then
						TDO <= iTxD_DATA(3);
					elsif rCont = x"4" then
						TDO <= iTxD_DATA(4);
					elsif rCont = x"5" then
						TDO <= iTxD_DATA(5);
					elsif rCont = x"6" then
						TDO <= iTxD_DATA(6);
					elsif rCont = x"7" then
						TDO <= iTxD_DATA(7);
					end if;
				else
					rCont<=O"0";
					TDO<='0';
				end if; 
				if(rCont=O"7") then
					oTxD_Done<='1';
				else
					oTxD_Done<='0';
				end if; 
			end if; 
		--end if; 
	end process; 
end jt;