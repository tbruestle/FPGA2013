
library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

Entity USB_JTAG is
port(	
      iTxD_DATA   :  in std_logic_vector(7 downto 0);
      iTxD_Start  :  in std_logic;
      iRST_n      :  in std_logic;
      iCLK        :  in std_logic;
      TDI         :  in std_logic;
      TCS         :  in std_logic;
      TCK         :  in std_logic;
      oRxD_DATA   : out std_logic_vector(7 downto 0);
      oTxD_Done   : out std_logic;
      oRxD_Ready  : out std_logic;
      TDO         : out std_logic
);
end USB_JTAG;

architecture usbj of USB_JTAG is
	signal mRxD_DATA : std_logic_vector(7 downto 0);
	signal Pre_TxD_Done :  std_logic;
	signal Pre_RxD_Ready :  std_logic;
	signal mTxD_Done :  std_logic;
	signal mRxD_Ready :  std_logic;
	signal mTCK :  std_logic;
	
	COMPONENT JTAG_REC
	PORT
	(
		TDI : in std_logic;
		TCS : in std_logic;
		TCK : in std_logic;
		oRxD_DATA : out std_logic_vector(7 downto 0);
		oRxD_Ready : out std_logic
	);
	END COMPONENT;
	
	COMPONENT JTAG_TRANS
	PORT
	(
		iTxD_DATA : in std_logic_vector(7 downto 0);
		iTxD_Start : in std_logic;
		oTxD_Done : out std_logic;
		TDO : out std_logic;
		TCK : in std_logic;
		TCS : in std_logic
	);
	END COMPONENT;
	
begin

	------------	JTAG CLK Sync.	--------------

		process(iCLK) 
		begin 
			if iCLK'event and iCLK='1'then 
				mTCK<=TCK;
			end if; 
		end process; 
	----------------	JTAG Receiver	--------------
		
	u0: JTAG_REC PORT MAP
	(
		oRxD_DATA => mRxD_DATA,
		oRxD_Ready => mRxD_Ready,
		TDI => TDI,
		TCS => TCS,
		TCK => mTCK
		);
	--	JTAG Receiver Sync.
	--------------------------------------------------/
	process(iCLK, iRST_n) 
	begin
		if (iRST_n='0') then
			oRxD_Ready<='0';
			Pre_RxD_Ready<='0';
		elsif (iCLK'event and iCLK='1') then
			Pre_RxD_Ready <= mRxD_Ready;
			if (Pre_RxD_Ready='0' and  mRxD_Ready = '1' and iTxD_Start='0') then
				oRxD_Ready<='1';
				oRxD_DATA<=mRxD_DATA;
			else
				oRxD_Ready<='0';
			end if; 
		end if; 
	end process; 
	------------/	JTAG Transmitter	--------------/
	u1: JTAG_TRANS PORT MAP
	(
		iTxD_DATA => iTxD_DATA,
		iTxD_Start => iTxD_Start,
		oTxD_Done => mTxD_Done,
		TDO => TDO,
		TCK => TCK,
		TCS => TCS
		);
	--	JTAG Transmitter Sync.
	process(iCLK, iRST_n) 
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if (iRST_n='0') then
				oTxD_Done<='0';
				Pre_TxD_Done<='0';
			elsif (iCLK'event and iCLK='1') then
				Pre_TxD_Done<=mTxD_Done;
				if (Pre_TxD_Done='0' and  mTxD_Done = '1') then
					oTxD_Done<='1';
				else
					oTxD_Done<='0';
				end if; 
			end if; 
		--end if; 
	end process; 
	--------------------------------------------------/

end usbj;