-- ECE 8455 
-- TEMPLATE FOR TESTING JTAG PORT BETWEEN THE PC AND DE2-35 BOARD

library ieee;
use ieee.std_logic_1164.all;

entity DE2_USB_API is
  port (
    
    -- Clock Input
    CLOCK_50      : in std_logic;  -- 50 MHz
    
    -- Push Button
    KEY         : in std_logic_vector(3 downto 0); -- Buttons 3,2,1,0

    -- LED
    LEDG   : out std_logic_vector(8 downto 0);   -- LED Green, 8:0
    LEDR     : out std_logic_vector(17 downto 0);  -- LED Red, 17:0

    
    -- SRAM Interface
    SRAM_DQ     : inout std_logic_vector(15 downto 0); -- SRAM Data bus 16 Bits
    SRAM_ADDR   :   out std_logic_vector(17 downto 0); -- SRAM Address bus 18 Bits
    SRAM_UB_N   :   out std_logic; -- SRAM High-byte Data Mask
    SRAM_LB_N   :   out std_logic; -- SRAM Low-byte Data Mask
    SRAM_WE_N   :   out std_logic; -- SRAM Write Enable
    SRAM_CE_N   :   out std_logic; -- SRAM Chip Enable
    SRAM_OE_N   :   out std_logic; -- SRAM Output Enable
    
    -- USB JTAG link
    TDI         :    in std_logic; -- CPLD -> FPGA (data in)
    TCK         :    in std_logic; -- CPLD -> FPGA (clk)
    TMS         :    in std_logic; -- CPLD -> FPGA (CS)
    TDO         :   out std_logic -- FPGA -> CPLD (data out)
  );
end entity DE2_USB_API;

architecture structure of DE2_USB_API is
  -- USB JTAG
  signal mRxD_DATA       : std_logic_vector(7 downto 0);
  signal mTXD_DATA       : std_logic_vector(7 downto 0);
  signal mTxD_Done       : std_logic;
  signal mRxD_Ready      : std_logic;
  signal mTxD_Start      : std_logic;
  signal mTCK            : std_logic;

  -- SRAM
  signal mSR_ADDR         : std_logic_vector(17 downto 0);
  signal mSR2RS_DATA      : std_logic_vector(15 downto 0);
  signal mRS2SR_DATA      : std_logic_vector(15 downto 0);
  signal mSR_OE           : std_logic;
  signal mSR_WE           : std_logic;

  --Async Side 1
  signal mAS1_ADDR         : std_logic_vector(17 downto 0);
  signal mAS12RS_DATA      : std_logic_vector(15 downto 0);
  signal mRS2AS1_DATA      : std_logic_vector(15 downto 0);
  signal mAS1_OE           : std_logic;
  signal mAS1_WE           : std_logic;

  signal mSR_Select_CMD_DEC: std_logic_vector(2 downto 0); -- output byte enable from CMD Decode
  signal mSR_Select_SYS_CTRL: std_logic_vector(1 downto 0); -- output byte enable from System Control
  --signal SR_WRITE_DONE : std_logic; -- Used to signal that write to SRAM from Control Panel is complete

  -- Async Port Select
  signal mSR_Select        : std_logic_vector(1 downto 0); -- SRAM Byte Select

  
  component USB_JTAG is
    port (
      iTxD_DATA   :  in std_logic_vector(7 downto 0);
      iTxD_Start  :  in std_logic;
      iRST_n      :  in std_logic;
      iCLK        :  in std_logic;
      TDI         :  in std_logic;
      TCS         :  in std_logic;
      TCK         :  in std_logic;
      oRxD_DATA   : out std_logic_vector(7 downto 0);
      oTxD_Done   : out std_logic;
      oRxD_Ready  : out std_logic;
      TDO         : out std_logic
    );
  end component;

  component CMD_Decode is
  port (

	--	USB JTAG
	iRXD_DATA : in std_logic_vector(7 downto 0);
	iRXD_Ready : in std_logic;
	iTXD_Done : in std_logic;
	oTXD_DATA : out std_logic_vector(7 downto 0);
	oTXD_Start : out std_logic;
	--	LED
	oLED_RED : out std_logic_vector(17 downto 0);
	oLED_GREEN : out std_logic_vector(8 downto 0);
	--	7-SEG
	oSEG7_DIG : out std_logic_vector(31 downto 0);
	--	LCD
	oLCD_DATA : out std_logic_vector(7 downto 0);
	oLCD_RS : out std_logic;
	oLCD_Start : out std_logic;
	iLCD_Done : in std_logic;
	--	VGA
	oCursor_X : out std_logic_vector(9 downto 0);
	oCursor_Y : out std_logic_vector(9 downto 0);
	oCursor_R : out std_logic_vector(9 downto 0);
	oCursor_G : out std_logic_vector(9 downto 0);
	oCursor_B : out std_logic_vector(9 downto 0);
	oOSD_CUR_EN : out std_logic_vector(1 downto 0);
	--	FLASH
	iFL_DATA : in std_logic_vector(7 downto 0);
	iFL_Ready : in std_logic;
	oFL_ADDR : out std_logic_vector(21 downto 0);
	oFL_DATA : out std_logic_vector(7 downto 0);
	oFL_CMD : out std_logic_vector(2 downto 0);
	oFL_Start : out std_logic;
	--	SDRAM
	iSDR_DATA : in std_logic_vector(15 downto 0);
	iSDR_Done : in std_logic;
	oSDR_ADDR : out std_logic_vector(21 downto 0);
	oSDR_DATA : out std_logic_vector(15 downto 0);
	oSDR_WR : out std_logic;
	oSDR_RD : out std_logic;
	--	SRAM
	iSR_DATA : in std_logic_vector(15 downto 0);
	oSR_DATA : out std_logic_vector(15 downto 0);
	oSR_ADDR : out std_logic_vector(17 downto 0);
	oSR_WE_N : out std_logic;
	oSR_OE_N : out std_logic;
	--	PS2
	iPS2_ScanCode : in std_logic_vector(7 downto 0);
	iPS2_Ready : in std_logic;
	--	Async Port Select
	oSDR_Select : out std_logic_vector(1 downto 0);
	oFL_Select : out std_logic_vector(1 downto 0);
	oSR_Select : out std_logic_vector(1 downto 0);
	--	Ext Control Signals
	oExt_IO : out std_logic_vector(7 downto 0);
	--	Control
	iCLK : in std_logic;
	iRST_n : in std_logic;
	
	oSDR_TXD_Start : out std_logic;
	oPS2_TXD_Start : out std_logic;
	oSR_TXD_Start : out std_logic
  );
  end component;
  
 component multi_sram is
  port (
     -- Host Side
    iHS_ADDR  :    in std_logic_vector(17 downto 0);
    iHS_DATA  :    in std_logic_vector(15 downto 0);
    oHS_DATA  :   out std_logic_vector(15 downto 0);
    iHS_WE_N  :    in std_logic;
    iHS_OE_N  :    in std_logic;
    
    --Async Side 1
    iAS1_ADDR :    in std_logic_vector(17 downto 0);
    iAS1_DATA :    in std_logic_vector(15 downto 0);
    oAS1_DATA :   out std_logic_vector(15 downto 0);
    iAS1_WE_N :    in std_logic;
    iAS1_OE_N :    in std_logic;

    -- Control signals
    iSelect   :    in std_logic_vector(1 downto 0);
    iRST_n    :    in std_logic;
    
    -- SRAM Side
    SRAM_DQ   : inout std_logic_vector(15 downto 0);
    SRAM_ADDR :   out std_logic_vector(17 downto 0);
    SRAM_UB_N :   out std_logic;
    SRAM_LB_N :   out std_logic;
    SRAM_WE_N :   out std_logic;
    SRAM_CE_N :   out std_logic;
    SRAM_OE_N :   out std_logic
  );
end component;

begin
   

  usb_jtag0 : USB_JTAG
    port map(
      iTxD_DATA   => mTXD_DATA,
      iTxD_Start  => mTXD_Start,
      iRST_n      => KEY(0),
      iCLK        => CLOCK_50,
      TDI         => TDI,
      TCS         => TMS,
      TCK         => TCK,
      oRxD_DATA   => mRXD_DATA,
      oTxD_Done   => mTXD_Done,
      oRxD_Ready  => mRXD_Ready,
      TDO         => TDO
    );

  cmd_dec0 : CMD_Decode
    port map (
      iRXD_DATA    => mRXD_DATA,
      iRXD_Ready   => mRXD_Ready,
      iTXD_Done    => mTXD_Done,
      oTXD_DATA    => mTXD_DATA,
      oTXD_Start   => mTXD_Start,
      oLED_RED     => LEDR,
      oLED_GREEN   => LEDG,
      iSR_DATA     => mSR2RS_DATA,
      oSR_DATA     => mRS2SR_DATA,
      oSR_ADDR     => mSR_ADDR,
      oSR_WE_N     => mSR_WE,
      oSR_OE_N     => mSR_OE,
      oSR_Select   => mSR_Select_CMD_DEC(1 downto 0),
      --oExt_IO    => mExt_IO,
      iCLK         => CLOCK_50,
      iRST_n       => KEY(0),
		iLCD_Done	 => '0',
		iFL_DATA		 => x"00",
		iFL_Ready	 => '0',
		iSDR_DATA	 => x"0000",
		iSDR_Done	 => '0',
		iPS2_ScanCode=> x"00",
		iPS2_Ready	 => '0'
    );

  sram0 : multi_sram
    port map (
      iHS_ADDR  => mSR_ADDR,
      iHS_DATA  => mRS2SR_DATA,
      oHS_DATA  => mSR2RS_DATA,
      iHS_WE_N  => mSR_WE,
      iHS_OE_N  => mSR_OE,
      iAS1_ADDR => mAS1_ADDR,
      iAS1_DATA => mRS2AS1_DATA,
      oAS1_DATA => mAS12RS_DATA,
      iAS1_WE_N => mAS1_WE,
      iAS1_OE_N => mAS1_OE,
      iSelect   => mSR_Select,
      iRST_n    => KEY(0),
      SRAM_DQ   => SRAM_DQ,
      SRAM_ADDR => SRAM_ADDR,
      SRAM_UB_N => SRAM_UB_N,
      SRAM_LB_N => SRAM_LB_N,
      SRAM_WE_N => SRAM_WE_N,
      SRAM_CE_N => SRAM_CE_N,
      SRAM_OE_N => SRAM_OE_N
    );

 
end architecture structure;


