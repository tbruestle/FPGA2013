library ieee;
use ieee.std_logic_1164.all;

entity multi_sram is
  port (
     -- Host Side
    iHS_ADDR  :    in std_logic_vector(17 downto 0);
    iHS_DATA  :    in std_logic_vector(15 downto 0);
    oHS_DATA  :   out std_logic_vector(15 downto 0);
    iHS_WE_N  :    in std_logic;
    iHS_OE_N  :    in std_logic;
    
    --Async Side 1
    iAS1_ADDR :    in std_logic_vector(17 downto 0);
    iAS1_DATA :    in std_logic_vector(15 downto 0);
    oAS1_DATA :   out std_logic_vector(15 downto 0);
    iAS1_WE_N :    in std_logic;
    iAS1_OE_N :    in std_logic;
    
    -- Async Side 2
    --iAS2_ADDR :    in std_logic_vector(17 downto 0);
    --iAS2_DATA :    in std_logic_vector(15 downto 0);
    --oAS2_DATA :   out std_logic_vector(15 downto 0);
    --iAS2_WE_N :    in std_logic;
    --iAS2_OE_N :    in std_logic;
    
    -- Async Side 3
    --iAS3_ADDR :    in std_logic_vector(17 downto 0);
    --iAS3_DATA :    in std_logic_vector(15 downto 0);
    --oAS3_DATA :   out std_logic_vector(15 downto 0);
    --iAS3_WE_N :    in std_logic;
    --iAS3_OE_N :    in std_logic;
    
    -- Control signals
    iSelect   :    in std_logic_vector(1 downto 0);
    iRST_n    :    in std_logic;
    
    -- SRAM Side
    SRAM_DQ   : inout std_logic_vector(15 downto 0);
    SRAM_ADDR :   out std_logic_vector(17 downto 0);
    SRAM_UB_N :   out std_logic;
    SRAM_LB_N :   out std_logic;
    SRAM_WE_N :   out std_logic;
    SRAM_CE_N :   out std_logic;
    SRAM_OE_N :   out std_logic
  );
end entity multi_sram;

architecture behav of multi_sram is
  signal SRAM_WE_N_s : std_logic;
begin
  SRAM_DQ <= "ZZZZZZZZZZZZZZZZ" when SRAM_WE_N_s = '1' else
             iHS_DATA when iSelect = "00" else
             iAS1_DATA when iSelect = "01";-- else
             --iAS2_DATA when iSelect = "10" else
             --iAS3_DATA;
  
  oHS_DATA <= SRAM_DQ when iSelect = "00" else
              x"0000";
  
  oAS1_DATA <= SRAM_DQ when iSelect = "01" else
               x"0000";
  
  --oAS2_DATA <= SRAM_DQ when iSelect = "10" else
  --             x"0000";
  
  --oAS3_DATA <= SRAM_DQ when iSelect = "11" else
  --             x"0000";
  
  SRAM_ADDR <= iHS_ADDR when iSelect = "00" else
               iAS1_ADDR when iSelect = "01" else
               "ZZZZZZZZZZZZZZZZZZ";
               --iAS2_ADDR when iSelect = "10" else
               --iAS3_ADDR;
  
  SRAM_WE_N_s <= iHS_WE_N when iSelect = "00" else
                 iAS1_WE_N when iSelect = "01" else
                 '1';
                 --iAS2_WE_N when iSelect = "10" else
                 --iAS3_WE_N;
  
  SRAM_WE_N <= SRAM_WE_N_s;
  
  SRAM_OE_N <= iHS_OE_N when iSelect = "00" else
               iAS1_OE_N when iSelect = "01" else
               '1';
               --iAS2_OE_N when iSelect = "10" else
               --iAS3_OE_N;
  
  SRAM_CE_N <= '0';
  SRAM_UB_N <= '0';
  SRAM_LB_N <= '0';
  
end architecture behav;
