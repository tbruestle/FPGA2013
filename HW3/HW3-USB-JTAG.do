vcom -93 -explicit -work work usb_jtag.vhd
vsim usb_jtag
add wave -r /*
    
force iTxD_DATA 01100011 0 ns
force iTxD_Start 1 0 ns, 0 320 ns
force iRST_n 0 0 ns, 1 40 ns
force iCLK 0 0 ns, 1 10 ns -repeat 20 ns
force TDI 0 0 ns, 1 40 ns, 0 120 ns, 1 240 ns -repeat 320 ns
force TCS 1 0 ns, 0 60 ns
force TCK 0 0 ns, 1 20 ns -repeat 40 ns




run 800 ns