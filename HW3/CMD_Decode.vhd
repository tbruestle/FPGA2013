
library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

Entity CMD_Decode is
	port(	
	--	USB JTAG
	iRXD_DATA : in std_logic_vector(7 downto 0);
	iRXD_Ready : in std_logic;
	iTXD_Done : in std_logic;
	oTXD_DATA : out std_logic_vector(7 downto 0);
	oTXD_Start : out std_logic;
	--	LED
	oLED_RED : out std_logic_vector(17 downto 0);
	oLED_GREEN : out std_logic_vector(8 downto 0);
	--	7-SEG
	oSEG7_DIG : out std_logic_vector(31 downto 0);
	--	LCD
	oLCD_DATA : out std_logic_vector(7 downto 0);
	oLCD_RS : out std_logic;
	oLCD_Start : out std_logic;
	iLCD_Done : in std_logic;
	--	VGA
	oCursor_X : out std_logic_vector(9 downto 0);
	oCursor_Y : out std_logic_vector(9 downto 0);
	oCursor_R : out std_logic_vector(9 downto 0);
	oCursor_G : out std_logic_vector(9 downto 0);
	oCursor_B : out std_logic_vector(9 downto 0);
	oOSD_CUR_EN : out std_logic_vector(1 downto 0);
	--	FLASH
	iFL_DATA : in std_logic_vector(7 downto 0);
	iFL_Ready : in std_logic;
	oFL_ADDR : out std_logic_vector(21 downto 0);
	oFL_DATA : out std_logic_vector(7 downto 0);
	oFL_CMD : out std_logic_vector(2 downto 0);
	oFL_Start : out std_logic;
	--	SDRAM
	iSDR_DATA : in std_logic_vector(15 downto 0);
	iSDR_Done : in std_logic;
	oSDR_ADDR : out std_logic_vector(21 downto 0);
	oSDR_DATA : out std_logic_vector(15 downto 0);
	oSDR_WR : out std_logic;
	oSDR_RD : out std_logic;
	--	SRAM
	iSR_DATA : in std_logic_vector(15 downto 0);
	oSR_DATA : out std_logic_vector(15 downto 0);
	oSR_ADDR : out std_logic_vector(17 downto 0);
	oSR_WE_N : out std_logic;
	oSR_OE_N : out std_logic;
	--	PS2
	iPS2_ScanCode : in std_logic_vector(7 downto 0);
	iPS2_Ready : in std_logic;
	--	Async Port Select
	oSDR_Select : out std_logic_vector(1 downto 0);
	oFL_Select : out std_logic_vector(1 downto 0);
	oSR_Select : out std_logic_vector(1 downto 0);
	--	Ext Control Signals
	oExt_IO : out std_logic_vector(7 downto 0);
	--	Control
	iCLK : in std_logic;
	iRST_n : in std_logic;
	
	oSDR_TXD_Start : out std_logic;
	oPS2_TXD_Start : out std_logic;
	oSR_TXD_Start : out std_logic
	);
end CMD_Decode;



architecture cmdd of CMD_Decode is


	signal mRxD_DATA : std_logic_vector(7 downto 0);
	signal Pre_TxD_Done :  std_logic;
	signal Pre_RxD_Ready :  std_logic;
	signal mTxD_Done :  std_logic;
	signal mRxD_Ready :  std_logic;
	signal mTCK :  std_logic;
	
	--	Internal Register
	signal CMD_Tmp :  std_logic_vector(63 downto 0);
	signal mFL_ST :  std_logic_vector(2 downto 0);
	signal mSDR_ST :  std_logic_vector(2 downto 0);
	signal mPS2_ST :  std_logic_vector(2 downto 0);
	signal mSR_ST :  std_logic_vector(2 downto 0);
	signal mLCD_ST :  std_logic_vector(2 downto 0);
	--	SDRAM Control Register
	signal mSDR_WRn : std_logic;
	signal mSDR_Start : std_logic;
	--	SRAM Control Register
	signal mSR_WRn : std_logic;
	signal mSR_Start : std_logic;
	--	Active Flag
	signal f_SETUP : std_logic;
	signal f_LED : std_logic;
	signal f_SEG7 : std_logic;
	signal f_SDR_SEL : std_logic;
	signal f_FL_SEL : std_logic;
	signal f_SR_SEL : std_logic;
	signal f_FLASH : std_logic;
	signal f_SDRAM : std_logic;
	signal f_PS2 : std_logic;
	signal f_EXT_IO : std_logic;
	signal f_SRAM : std_logic;
	signal f_LCD : std_logic;
	signal f_VGA : std_logic;
	--	USB JTAG TXD;
	signal oFL_TXD_Start : std_logic;
	signal oFL_TXD_DATA : std_logic_vector(7 downto 0);
	signal oSDR_TXD_DATA : std_logic_vector(7 downto 0);
	signal oPS2_TXD_DATA : std_logic_vector(7 downto 0);
	signal oSR_TXD_DATA : std_logic_vector(7 downto 0);
	--	TXD Output Select Register
	signal sel_FL : std_logic;
	signal sel_SDR : std_logic;
	signal sel_PS2 : std_logic;
	signal sel_SR : std_logic;
	--FLASH
	
	signal oFL_CMD_temp : std_logic_vector(2 downto 0);


	signal CMD_Action : std_logic_vector(7 downto 0);
	signal CMD_Target : std_logic_vector(7 downto 0);
	signal CMD_ADDR : std_logic_vector(23 downto 0);
	signal CMD_DATA : std_logic_vector(15 downto 0);
	signal CMD_MODE : std_logic_vector(7 downto 0);
	signal Pre_Target : std_logic_vector(7 downto 0);
	
	-------------   Controller Command	------------------------
constant CMD_READ : std_logic_vector(3 downto 0) :=	x"0";
constant CMD_WRITE : std_logic_vector(3 downto 0) :=	x"1";
constant CMD_BLK_ERA : std_logic_vector(3 downto 0) :=	x"2";
constant CMD_SEC_ERA : std_logic_vector(3 downto 0) :=	x"3";
constant CMD_CHP_ERA : std_logic_vector(3 downto 0) :=	x"4";
constant CMD_ENTRY_ID : std_logic_vector(3 downto 0) :=	x"5";
constant CMD_RESET : std_logic_vector(3 downto 0) :=	x"6";
------------------  Command Action  ---------------------
constant   SETUP   : std_logic_vector(7 downto 0) :=   x"61";
constant   ERASE   : std_logic_vector(7 downto 0) :=   x"72";
constant   WRITE   : std_logic_vector(7 downto 0) :=   x"83";
constant   READ    : std_logic_vector(7 downto 0) :=   x"94";
constant   LCD_DAT : std_logic_vector(7 downto 0) :=   x"83";
constant   LCD_CMD : std_logic_vector(7 downto 0) :=   x"94";
------------------  Command Target  ---------------------
constant   LED     : std_logic_vector(7 downto 0) :=   x"F0";
constant   SEG7    : std_logic_vector(7 downto 0) :=   x"E1";
constant   PS2     : std_logic_vector(7 downto 0) :=   x"D2";
constant   FLASH   : std_logic_vector(7 downto 0) :=   x"C3";
constant   SDRAM   : std_logic_vector(7 downto 0) :=   x"B4";
constant   SRAM    : std_logic_vector(7 downto 0) :=   x"A5";
constant   LCD     : std_logic_vector(7 downto 0) :=   x"96";
constant   VGA     : std_logic_vector(7 downto 0) :=   x"87";
constant   SDRSEL  : std_logic_vector(7 downto 0) :=   x"1F";
constant   FLSEL   : std_logic_vector(7 downto 0) :=   x"2E";
constant   EXTIO   : std_logic_vector(7 downto 0) :=   x"3D";
constant   SET_REG : std_logic_vector(7 downto 0) :=   x"4C";
constant   SRSEL   : std_logic_vector(7 downto 0) :=   x"5B";
------------------  Command Mode    ---------------------
constant   OUTSEL  : std_logic_vector(7 downto 0) :=   x"33";
constant   NORMAL  : std_logic_vector(7 downto 0) :=   x"AA";
constant   DISPLAY : std_logic_vector(7 downto 0) :=   x"CC";
constant   BURST   : std_logic_vector(7 downto 0) :=   x"FF";

begin

		CMD_Action	<=	CMD_Tmp(63 downto 56);
		CMD_Target	<=	CMD_Tmp(55 downto 48);
		CMD_ADDR	<=	CMD_Tmp(47 downto 24);
		CMD_DATA	<=	CMD_Tmp(23 downto 8);
		CMD_MODE	<=	CMD_Tmp(7 downto 0);
		Pre_Target	<=	CMD_Tmp(47 downto 40);
		oFL_CMD <= oFL_CMD_temp;
--	process
--	begin
--	end process;


	--------------------------------------------------------/
	----------------	 SDRAM Select	--------------------/
	process(iCLK, iRST_n)
	begin
		if iRST_n='0' then
			oSDR_Select	<= B"00";
			f_SDR_SEL	<= '0';
		elsif (iCLK'event and iCLK='1') then 
			if(iRXD_Ready='1' and (Pre_Target = SDRSEL) ) then
				f_SDR_SEL<= '1';
			end if;
			if f_SDR_SEL = '1' then
				if (CMD_Action	= SETUP) and (CMD_MODE	= OUTSEL) and ((CMD_ADDR = x"123456") ) then
					oSDR_Select<=CMD_DATA(1 downto 0);
				end if;
				f_SDR_SEL<= '0';
			end if;
		end if;
	end process;


	--------------------------------------------------------/
	----------------	 FLASH Select	--------------------/
	process(iCLK, iRST_n)
	begin
		 --if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oFL_Select	<= B"00";
				f_FL_SEL	<= '0';
			elsif (iCLK'event and iCLK='1') then
				if(iRXD_Ready='1' and (Pre_Target = FLSEL) ) then
					f_FL_SEL<= '1';
				end if;
				if f_FL_SEL='1' then
					if ( (CMD_Action	= SETUP) and (CMD_MODE	= OUTSEL) ) and ((CMD_ADDR = x"123456") ) then
						oFL_Select<=CMD_DATA(1 downto 0);
					end if;
					f_FL_SEL<= '0';
				end if;
			end if;
		--end if;
	end process;


	--------------------------------------------------------/
	----------------	 SRAM Select	--------------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oSR_Select	<= B"00";
				f_SR_SEL	<= '0';
			elsif (iCLK'event and iCLK='1') then
				if(iRXD_Ready = '1' and (Pre_Target = SRSEL) ) then
					f_SR_SEL<= '1';
				end if;
				if f_SR_SEL='1' then
					if ( (CMD_Action	= SETUP) and (CMD_MODE	= OUTSEL) ) and ((CMD_ADDR = x"123456") ) then
						oSR_Select<=CMD_DATA(1 downto 0);
					end if;
					f_SR_SEL<= '0';
				end if;
			end if;
		--end if;
	end process;
	--------------------------------------------------------/
	----------------	 External IO	--------------------/
	process(iCLK, iRST_n)
	begin
		 --if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oExt_IO	<= x"00";
				f_EXT_IO	<= '0';
			elsif (iCLK'event and iCLK='1') then
				if(iRXD_Ready = '1' and (Pre_Target = EXTIO) ) then
					f_EXT_IO<= '1';
				end if;
				if f_EXT_IO = '1' then
					if ( (CMD_Action	= WRITE) and (CMD_MODE	= NORMAL) ) and ((CMD_ADDR = x"123456") ) then
						oExt_IO<=CMD_DATA(7 downto 0);
					end if;
					f_EXT_IO<= '0';
				end if;
			end if;
		--end if;
	end process;
	--------------------------------------------------------/
	------/		Shift Register For Command Temp	------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				CMD_Tmp<= x"0000000000000000";
			elsif (iCLK'event and iCLK='1') then
				if iRXD_Ready = '1' then
					CMD_Tmp(63 downto 0 ) <= CMD_Tmp(55 downto 0) & iRXD_DATA;
				end if;
			end if;
		--end if;
	end process;
	--------------------------------------------------------/
	----------------	 LED Control	--------------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oLED_RED	<= O"000000";
				oLED_GREEN	<= O"000";
				f_LED <= '0';
			elsif (iCLK'event and iCLK='1') then
				if(iRXD_Ready = '1' and Pre_Target = LED ) then
					f_LED<= '1';
				end if;
				if f_LED = '1' then
					if ( (CMD_Action	= WRITE) and (CMD_MODE	= DISPLAY) ) then
						oLED_RED	<=CMD_ADDR(17 downto 0);
						oLED_GREEN	<=CMD_DATA(8 downto 0);
					end if;
					f_LED<= '0';
				end if;
			end if;
		--end if;
	end process;
	--------------------------------------------------------/
	----------------	7-SEG Control	--------------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oSEG7_DIG<= x"00000000";
				f_SEG7<= '0';
			elsif (iCLK'event and iCLK='1') then
				if(iRXD_Ready = '1' and (Pre_Target = SEG7) ) then
					f_SEG7<= '1';
				end if;
				if f_SEG7='1' then
					if ( (CMD_Action	= WRITE) and (CMD_MODE	= DISPLAY) ) then
						oSEG7_DIG <= CMD_ADDR(15 downto 0) & CMD_DATA;
					end if;
					f_SEG7<= '0';			
				end if;
			end if;
		--end if;
	end process;
	--------------------------------------------------------/
	----------------	Flash Control	--------------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oFL_TXD_Start	<= '0';
				oFL_Start		<= '0';
				f_FLASH			<= '0';
				mFL_ST			<= O"0";
			elsif (iCLK'event and iCLK='1') then
				if( CMD_Action = READ ) then
					oFL_CMD_temp		<=	CMD_READ(2 downto 0);
				elsif ( CMD_Action = WRITE ) then
					oFL_CMD_temp		<=	CMD_WRITE(2 downto 0);
				elsif ( CMD_Action = ERASE ) then
					oFL_CMD_temp		<=	CMD_CHP_ERA(2 downto 0);
				else
					oFL_CMD_temp		<=	"000";
				end if;
				
				if(iRXD_Ready='1' and (Pre_Target = FLASH)) then
					f_FLASH<= '1';
				end if;
				if f_FLASH='1' then
					if mFL_ST=O"0" then
						if  (CMD_MODE	= NORMAL) and (CMD_Target = FLASH) and (CMD_DATA(15 downto 8) = x"FF")  then
							oFL_ADDR	<=	CMD_ADDR(21 downto 0);
							oFL_DATA	<=	CMD_DATA(7 downto 0);
							oFL_Start<= '1';
							mFL_ST	<= O"1";
						else
							mFL_ST	<= O"0";
							f_FLASH	<= '0';
						end if;
					elsif mFL_ST=O"1" then
						if iFL_Ready='1' then
							mFL_ST<=O"2";
							oFL_Start<= '0';
						end if;
					elsif mFL_ST=O"2" then
						oFL_Start<= '1';
						mFL_ST<=O"3";
					elsif mFL_ST=O"3" then
						if iFL_Ready='1' then
							mFL_ST<=O"4";
							oFL_Start<= '0';
						end if;
					elsif mFL_ST=O"4" then
						oFL_Start<= '1';
						mFL_ST<=O"5";
					elsif mFL_ST=O"5" then
						if iFL_Ready='1' then
							if oFL_CMD_temp = CMD_READ then
								mFL_ST	<=	O"6";
							else
								mFL_ST	<= O"0";
								f_FLASH	<= '0';	
							end if;
							oFL_Start	<= '0';
						end if;
					elsif mFL_ST=O"6" then
						oFL_TXD_DATA	<=	iFL_DATA;
						oFL_TXD_Start	<= '1';
						mFL_ST			<=	O"7";
					elsif mFL_ST=O"7" then
						if iTXD_Done='1' then
							oFL_TXD_Start<= '0';
							mFL_ST	<= O"0";
							f_FLASH	<= '0';
						end if;
					end if;
				end if;
			end if;
		--end if;
	end process;
	--------------------------------------------------------/
	----------------/	PS2 Control		--------------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oPS2_TXD_Start<= '0';
				f_PS2<= '0';
				mPS2_ST<= O"0";
			elsif (iCLK'event and iCLK='1') then
				if iPS2_Ready = '1' and iPS2_ScanCode /= x"2e" then
					f_PS2<= '1';
					oPS2_TXD_DATA<=iPS2_ScanCode;
				end if;
				if f_PS2='1' then
					if mPS2_ST= O"0" then
							oPS2_TXD_Start	<= '1';
							mPS2_ST			<= O"1";
					elsif mPS2_ST=O"1" then
							if iTXD_Done='1' then
								oPS2_TXD_Start	<= '0';
								mPS2_ST			<= O"0";
								f_PS2			<= '0';
							end if;
					end if;
				end if;
			end if;
		--end if;
	end process;
	--------------------------------------------------------/
	----------------	Sdram Control	--------------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oSDR_TXD_Start	<= '0';
				mSDR_WRn		<= '0';
				mSDR_Start		<= '0';
				f_SDRAM			<= '0';
				mSDR_ST			<= O"0";
			elsif (iCLK'event and iCLK='1') then
				if ( CMD_Action = READ ) then
					mSDR_WRn	<=	'0';
				elsif ( CMD_Action = WRITE ) then
					mSDR_WRn	<=	'1';
				end if;
				
				if (iRXD_Ready='1' and (Pre_Target = SDRAM)) then
					f_SDRAM<= '1';
				end if;
				if f_SDRAM = '1' then
					if mSDR_ST = O"0" then
							if  (CMD_MODE	= NORMAL) and (CMD_Target = SDRAM)  then
								oSDR_ADDR	<=	CMD_ADDR(21 downto 0);
								oSDR_DATA	<=	CMD_DATA;
								mSDR_Start	<= '1';
								mSDR_ST		<= O"1";			
							else
								mSDR_ST	<= O"0";
								f_SDRAM	<= '0';
							end if;
					elsif mSDR_ST = O"1" then
							if iSDR_Done='1' then
								if(mSDR_WRn = '0') then
									mSDR_ST	<=	O"2";
								else
									mSDR_ST	<= O"0";
									f_SDRAM	<= '0';							
									mSDR_Start	<= '0';
								end if;
							end if;
					elsif mSDR_ST = O"2" then
							oSDR_TXD_DATA	<= iSDR_DATA(7 downto 0);
							oSDR_TXD_Start	<= '1';
							mSDR_ST			<=	O"3";
					elsif mSDR_ST = O"3" then
							if iTXD_Done='1' then
								oSDR_TXD_Start<= '0';
								mSDR_ST	<=	O"4";
							end if;
					elsif mSDR_ST = O"4" then
							oSDR_TXD_DATA	<= 	iSDR_DATA(15 downto 8);
							oSDR_TXD_Start	<= '1';
							mSDR_ST			<=	O"5";
					elsif mSDR_ST = O"5" then
							if iTXD_Done='1' then
								mSDR_Start	<= '0';
								oSDR_TXD_Start<= '0';
								mSDR_ST	<= O"0";
								f_SDRAM	<= '0';
							end if;
					end if;			
				end if;
			end if;
		--end if;
	end process;

	oSDR_WR	<=	mSDR_WRn and mSDR_Start;
	oSDR_RD	<=	(not mSDR_WRn) and mSDR_Start;
	--------------------------------------------------------/
	----------------	SRAM Control	--------------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oSR_TXD_Start	<= '0';
				mSR_WRn			<= '0';
				mSR_Start		<= '0';
				f_SRAM			<= '0';
				mSR_ST			<= O"0";
			elsif (iCLK'event and iCLK='1') then
				if CMD_Action = READ then
					mSR_WRn	<=	'0';
				elsif CMD_Action = WRITE then
					mSR_WRn	<=	'1';
				end if;
				
				if(iRXD_Ready='1' and Pre_Target = SRAM) then
					f_SRAM<= '1';
				end if;
				if f_SRAM='1' then
					if( mSR_ST = O"0" ) then
						if  (CMD_MODE	= NORMAL) and (CMD_Target = SRAM)  then
							oSR_ADDR	<=	CMD_ADDR(17 downto 0);
							oSR_DATA	<=	CMD_DATA;
							mSR_Start <= '1';
							mSR_ST <= O"1";
						else
							mSR_ST	<= O"0";
							f_SRAM	<= '0';	
						end if;
					elsif( mSR_ST = O"1" ) then
						if (mSR_WRn = '0') then
							mSR_ST <= O"2";
						else
							mSR_ST <= O"0";
							f_SRAM <= '0';							
							mSR_Start <= '0';	
						end if;
					elsif( mSR_ST = O"2" ) then
						oSR_TXD_DATA <= iSR_DATA(7 downto 0);
						oSR_TXD_Start <= '1';
						mSR_ST <= O"3";
					elsif( mSR_ST = O"3" ) then
						if iTXD_Done='1' then
							oSR_TXD_Start <= '0';
							mSR_ST <= O"4";	
						end if;
					elsif( mSR_ST = O"4" ) then
							oSR_TXD_DATA <= iSR_DATA(15 downto 8);
							oSR_TXD_Start <= '1';
							mSR_ST <= O"5";
					elsif( mSR_ST = O"5" ) then
						if iTXD_Done='1' then
							mSR_Start	<= '0';
							oSR_TXD_Start<= '0';
							mSR_ST		<= O"0";
							f_SRAM		<= '0';	
						end if;
					end if;
				end if;
			end if;
		--end if;
	end process;

	oSR_OE_N	<=	not( not mSR_WRn and mSR_Start );
	oSR_WE_N	<=	not( mSR_WRn and mSR_Start );

	--------------------------------------------------------/
	----------------------	LCD Control	--------------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oLCD_RS			<= '0';
				oLCD_Start		<= '0';
				f_LCD			<= '0';
				mLCD_ST			<= O"0";
			elsif (iCLK'event and iCLK='1') then
				if( CMD_Action = LCD_CMD ) then
					oLCD_RS	<=	'0';
				elsif( CMD_Action = LCD_DAT ) then
					oLCD_RS	<=	'1';
				end if;
				
				if(iRXD_Ready='1' and Pre_Target = LCD) then
					f_LCD<= '1';
				end if;
				if f_LCD='1' then
					if( mLCD_ST = 0 ) then
						if (CMD_MODE	= DISPLAY) and (CMD_Target = LCD)  and (CMD_ADDR = x"123456")  then
							oLCD_DATA	<=	CMD_DATA(7 downto 0);
							oLCD_Start	<= '1';
							mLCD_ST		<= O"1";
						else
							mLCD_ST	<= O"0";
							f_LCD	<= '0';
						end if;
					elsif( mLCD_ST = 1 ) then
						if iLCD_Done='1' then
							mLCD_ST	<= O"0";
							f_LCD	<= '0';							
							oLCD_Start	<= '0';
						end if;
					end if;
				end if;
			end if;
		--end if;
	end process;
	--------------------------------------------------------/
	--------------------   VGA Control	--------------------/
	process(iCLK, iRST_n)
	begin
		--if (iCLK'event and iCLK='1') or (iRST_n'event and iRST_n='0') then 
			if iRST_n='0' then
				oCursor_X	<= "0000000000";
				oCursor_Y	<= "0000000000";
				oCursor_R	<= "0000000000";
				oCursor_G	<= "0000000000";
				oCursor_B	<= "0000000000";
				oOSD_CUR_EN	<= "00";
				f_VGA		<= '0';
			elsif (iCLK'event and iCLK='1') then
				if(iRXD_Ready='1'  and Pre_Target = VGA ) then
					f_VGA<= '1';	
				end if;
				if f_VGA='1' then
					if  (CMD_Action	= WRITE) and (CMD_MODE	= DISPLAY)  then
						if CMD_ADDR(2 downto 0) = x"0" then
							oOSD_CUR_EN	<=	CMD_DATA(1 downto 0);
						elsif CMD_ADDR(2 downto 0) = x"1" then
							oCursor_X	<=	CMD_DATA(9 downto 0);
						elsif CMD_ADDR(2 downto 0) = x"2" then
							oCursor_Y	<=	CMD_DATA(9 downto 0);
						elsif CMD_ADDR(2 downto 0) = x"3" then
							oCursor_R	<=	CMD_DATA(9 downto 0);
						elsif CMD_ADDR(2 downto 0) = x"4" then	
							oCursor_G	<=	CMD_DATA(9 downto 0);
						elsif CMD_ADDR(2 downto 0) = x"5" then
							oCursor_B	<=	CMD_DATA(9 downto 0);	
						end if;
					end if;
					f_VGA<= '0';		
				end if;
			end if;
		--end if;
	end process;
	--------------------------------------------------------/

end cmdd;