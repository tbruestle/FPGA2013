vcom -93 -explicit -work work cmd_decode.vhd
vsim cmd_decode
    
force iCLK 0 0 ns, 1 10 ns -repeat 20 ns
force iRST_n 0 0 ns, 1 20 ns

force iRXD_DATA 00000000 0 ns, 01100001 20 ns, 00101110 40 ns, 00010010 60 ns, 00110100 80 ns, 01010110 100 ns, 00000000 120 ns, 00000011 140 ns, 00110011 160 ns
                ## reset       ## CMD_Action = x61 (SETUP)     ## CMD_ADDR(23 downto 16) = x12 ## CMD_ADDR(7 downto 0) = x56     ## CMD_DATA(7 downto 0) = 000000 + oFL_Select
                                               ## CMD_Target	= x2E (FLSEL)    ## CMD_ADDR(15 downto 8) = x34   ## CMD_DATA(15 downto 8) = 0      ## CMD_MODE	= x33 (OUTSEL)
 
force iRXD_Ready 0 0 ns, 1 20 ns, 0 180 ns


add wave /iCLK
add wave /iRST_n
add wave /iRXD_DATA
add wave /iRXD_Ready


add wave /CMD_Tmp


add wave /CMD_Action	
add wave /CMD_Target	
add wave /CMD_ADDR	
add wave /CMD_DATA	
add wave /CMD_MODE		

add wave /Pre_Target	
	
add wave /f_FL_SEL

add wave /oFL_Select


run 200 ns