library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

Entity JTAG_REC is
port(	
--	JTAG
	TDI : in std_logic;
	TCS : in std_logic;
	TCK : in std_logic;
--	HOST	
	oRxD_DATA : out std_logic_vector(7 downto 0);
	oRxD_Ready : out std_logic
);
end JTAG_REC;

architecture jr of JTAG_REC is
	signal rDATA : std_logic_vector(7 downto 0);
	signal rCont  : std_logic_vector(2 downto 0);
begin

	process(TCK, TCS) 
	begin
		if (TCS='1') then
			oRxD_Ready<='0';
			rCont <= o"0";
		elsif (TCK'event and TCK='1') then
			rCont<=rCont+1;
			rDATA(7)<=TDI;
			rDATA(6 downto 0)<=rData(7 downto 1);
			if (rCont=x"0") then
				oRxD_DATA(7)<=TDI;
				oRxD_DATA(6 downto 0)<=rData(7 downto 1);
				oRxD_Ready<='1';
			else
				oRxD_Ready<='0';
			end if; 
		end if; 
	end process; 
	
end jr;