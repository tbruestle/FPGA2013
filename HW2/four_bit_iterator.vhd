-- File Name: 4_bit_iterator.vhd
-- Module : four bit iterator with reset and add_sub signal
-- Computes:
-- (sum) = (a + 1 - (add_sub * 2) ) * ( 1 - reset)
library ieee;
use ieee.std_logic_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;


entity four_bit_iterator is
port(
	reset : in std_logic;
	add_sub : in std_logic;
	enable : in std_logic;
	clk : in std_logic;
	sum : out std_logic_vector(3 downto 0)
);
end four_bit_iterator;

architecture struc of four_bit_iterator is
	signal tempsum: std_logic_vector(3 downto 0);
begin
	process(clk, tempsum) 
	begin 
		if clk'event and clk='1' then 
			if reset='1' then 
			   tempsum <= "0000";
			elsif enable='0' then
			   tempsum <= tempsum;
			elsif add_sub='0' then
			  if tempsum < 15 then
			   tempsum <= tempsum + 1;
			  else
			   tempsum <= "0000";
			  end if;
			elsif add_sub='1' then
			  if tempsum > 0 then
			   tempsum <= tempsum - 1;
			  else
			   tempsum <= "1111";
			  end if;
			end if; 
		end if; 
		sum <= tempsum;
	end process; 
end struc;