-- File Name: 4_bit_iterator.vhd
-- Module : four bit iterator with reset and add_sub signal
-- Computes:
-- (sum) = (a + 1 - (add_sub * 2) ) * ( 1 - reset)
library ieee;
use ieee.std_logic_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;


entity full_project is
port(
	SW : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	CLOCK_50 : in std_logic;
	HEX0 : out bit_vector (0 to 6)
);
end full_project;

architecture struc1 of full_project is

	signal sum: std_logic_vector(3 downto 0) := ( 3=>'0',2=>'0',1=>'0',0=>'0');
	signal clock_1MHz	: std_logic;
	signal clock_100KHz : std_logic;
	signal clock_10KHz : std_logic;
	signal clock_1KHz	: std_logic;
	signal clock_100Hz : std_logic;
	signal clock_10Hz	: std_logic;
	signal clock_1Hz : std_logic;
	
	COMPONENT four_bit_iterator
	PORT
	(
	reset : in std_logic;
	add_sub : in std_logic;
	enable : in std_logic;
	clk : in std_logic;
	sum : out std_logic_vector(3 downto 0)
	);
	END COMPONENT;
	
	
	COMPONENT clk_div
	PORT
	(
		clock_50Mhz			: IN	STD_LOGIC;
		clock_1MHz				: OUT	STD_LOGIC;
		clock_100KHz			: OUT	STD_LOGIC;
		clock_10KHz				: OUT	STD_LOGIC;
		clock_1KHz				: OUT	STD_LOGIC;
		clock_100Hz				: OUT	STD_LOGIC;
		clock_10Hz				: OUT	STD_LOGIC;
		clock_1Hz				: OUT	STD_LOGIC
		);
	END COMPONENT;
	
	COMPONENT seven_led
	PORT(	
		bin_in:	in bit_vector (3 downto 0);
		hex_out:	out bit_vector (0 to 6)
		);
	END COMPONENT; 

begin

	clk1: clk_div PORT MAP
	(
		clock_50Mhz => CLOCK_50,
		clock_1MHz => clock_1MHz,
		clock_100KHz => clock_100KHz,
		clock_10KHz => clock_10KHz,
		clock_1KHz => clock_1KHz,
		clock_100Hz => clock_100Hz,
		clock_10Hz => clock_10Hz,
		clock_1Hz => clock_1Hz
		);
		
		
	fbi1: four_bit_iterator PORT MAP
	(
	reset => SW(0),
	add_sub => SW(1),
	enable => SW(2),
	clk =>  clock_1Hz,
	sum => sum
	);

--	process(CLOCK_50) 
--	begin 
--		if CLOCK_50'event and CLOCK_50='1' then 
--			if sw[0]='1' then 
--			   sum <= "0000";
--			elsif sw[2]='0' then
--			   sum <= sum;
--			elsif sw[1]='0' then
--			  if sum < 15 then
--			   sum <= sum + 1;
--			  else
--			   sum <= "0000";
--			  end if;
--			elsif sw[1]='1' then
--			  if sum > 0 then
--			   sum <= sum - 1;
--			  else
--			   sum <= "1111";
--			  end if;
--			end if; 
--		end if; 
--	end process; 
	
	sled1: seven_led
	PORT MAP(	
		bin_in(3 downto 0) => to_bitvector(sum(3 downto 0)),
		hex_out(0 to 6) => HEX0(0 to 6)
		);
	
end struc1;