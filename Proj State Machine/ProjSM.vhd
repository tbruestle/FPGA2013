library ieee;
use ieee.std_logic_1164.all;

entity ProjSM is

	port(
		--word : in string (1 to 8); --This is the word input, preferably they will be 8 character words, so some may be spaces
		CLK : in std_logic;
		reset	 : in	std_logic
		
	);
	
end entity;

architecture behav of ProjSM is

	
	type state_type is ( s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12 ); --I cut it down to 12 states
	signal inputnum : std_logic_vector (0 to 2); --these are used to count which input or output we are on
	signal outputnum : std_logic_vector (0 to 2); --based on these numbers we will output it properly to the VGA
	signal temp_inout : string (1 to 8); --here I save the word before we know if it is in or out
	signal word : string (1 to 8); --This will be removed but I put it here so the compiler would work
	signal state : state_type;
	signal title,in1,in2,in3,in4,in5,in6,out1,out2,out3,out4,out5,out6 : string (1 to 8); --These are my outputs to the VGA
		--for some reason this declaration causes there to be 8 times the number of pins I need, so it's unable to compile
 		
begin
	-- Logic to advance to the next state
word  <= "ENTITY  " ;
	
	process (CLK ,reset, word, state, inputnum, temp_inout, outputnum) --i had to include all of these in the process statement, not sure if its the best way
	begin
	if CLK'event and CLK = '1' then
    if reset='1' then
			state <= s1;
		else               --I had the state triggered by a change in the word,because the words will be read on a clock pulse
			case state is
				when s1=>
					if (word = "library " or word = "use     ") then -- this state checks for a library or use
						state <= s2;
					elsif (word = "entity") then --if entity we go to state 3
						state <= s3;
					else 
						state <= s2;
					end if;
				when s2=>                 --state 2 waits until a ; then goes back to state 1
					if (word = ";       " ) then
						state <= s1;
					else 
						state <= s2;
					end if;
				when s3 =>       --because the first word after entity is the name of entity, i simply saved it here and then move to next state
				 	title <= word;
					state <= s4;
				when s4 =>       -- this checks for if and then goes to the next state
					if (word = "is      ") then
						state <= s5;
					else 
						state <= s4;
					end if;
				when s5 =>        --checks for port and then moves to next state
					if (word = "port    ") then
						state <= s6;
					else 
						state <= s5;
					end if;
				when s6 =>       --checks for ( then moves to next state
					if (word = "(       ") then
						state <= s7;
					else 
						state <= s6;
					end if;
				when s7 =>        -- checks for ) to end entity, if not it saves the next word as a temp_inout
					if (word = ")       ") then
						state <= s12;
					else 
						temp_inout <= word;
						state <= s8;
					end if;
				when s8 =>       -- checks for : after saving to temp_inout
					if (word = ":       ") then
						state <= s8;
					else 
						state <= s7;
					end if;
				when s9 =>    --this state will see if the temp_inout is an input or output and work accordingly
					if (word = "in      ") then
						state <= s10;
					elsif (word = "out     ") then
						state <= s11;
					else
						state <= s9;
					end if;
				when s10 =>    --this saves to input, if it is our first input, we save to in1 and then increase input num so next time we save to in2 and so on
					if (word = ";       " and inputnum = "000") then
						in1 <= temp_inout;
						inputnum <= "001";
						state <= s7;
					elsif (word = ";       " and inputnum = "001") then
						in2 <= temp_inout;
						inputnum <= "010";
						state <= s7;
					elsif (word = ";       " and inputnum = "010") then
						in3 <= temp_inout;
						inputnum <= "011";
						state <= s7;
					elsif (word = ";       " and inputnum = "011") then
						in4 <= temp_inout;
						inputnum <= "100";
						state <= s7;
					elsif (word = ";       " and inputnum = "100") then
						in5 <= temp_inout;
						inputnum <= "101";
						state <= s7;
					elsif (word = ";       " and inputnum = "101") then
						in6 <= temp_inout;
						inputnum <= "000";
						state <= s7;
					else 
						state <= s10;
					end if;
				when s11 =>  --this saves to output, if it is our first output, we save to out1 and then increase output num so next time we save to out2 and so on
					if (word = ";       " and outputnum = "000") then
						out1 <= temp_inout;
						outputnum <= "001";
						state <= s7;
					elsif (word = ";       " and outputnum = "001") then
						out2 <= temp_inout;
						outputnum <= "010";
						state <= s7;
					elsif (word = ";       " and outputnum = "010") then
						out3 <= temp_inout;
						outputnum <= "011";
						state <= s7;
					elsif (word = ";       " and outputnum = "011") then
						out4 <= temp_inout;
						outputnum <= "100";
						state <= s7;
					elsif (word = ";       " and outputnum = "100") then
						out5 <= temp_inout;
						outputnum <= "101";
						state <= s7;
					elsif (word = ";       " and outputnum = "101") then
						out6 <= temp_inout;
						outputnum <= "000";
						state <= s7;
					else 
						state <= s10;
					end if;
				when s12 =>  --this ends the parsing...i figured that once we see a ); that we can assume to be finished with ports
					if (word = ";       ") then
						state <= s1;
					else 
						state <= s12;
					end if;
			end case;
		end if;
	end if;
end process;
	
	
end behav;
