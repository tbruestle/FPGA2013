-- seven_led.vhd
--
-- Function: convert a 4-bit binary input into a HEX number

library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

Entity seven_led is
	port(	bin_in:	in bit_vector (3 downto 0);
			hex_out:	out bit_vector (0 to 6));
end seven_led;

architecture beha of seven_led is
begin
	with bin_in select
		hex_out<="0000001" when "0000",
					"1001111" when "0001",
					"0010010" when "0010",
					"0000110" when "0011",
					"1001100" when "0100",
					"0100100" when "0101",
					"0100000" when "0110",
					"0001111" when "0111",
					"0000000" when "1000",
					"0000100" when "1001",
					"0001000" when "1010",
					"1100000" when "1011",
					"0110001" when "1100",
					"1000010" when "1101",
					"0110000" when "1110",
					"0111000" when "1111";
end beha;
