-- full_HW5.vhd
--
-- Function: Implements an SRAM interface, displaying the address, input, and output in a HEX output
-- Created by Thomas Bruestle for HW5
-- 3/18/2013

LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

entity full_HW5 is
	port(
		SW: in std_logic_vector(17 downto 0);
		KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		--LEDR: out std_logic_vector(17 downto 0);
		
		
		-- Representative Display
		
		LEDG: out std_logic_vector(7 downto 0); --WE
		
		
		HEX7: out bit_vector(0 to 6); --address
		HEX6: out bit_vector(0 to 6); --address
		
		HEX5: out bit_vector(0 to 6); --memory input
		HEX4: out bit_vector(0 to 6); --memory input
		
		HEX1: out bit_vector(0 to 6); --memory output
		HEX0: out bit_vector(0 to 6); --memory output
		
		
		-- SRAM
		
		SRAM_ADDR: out std_logic_vector(19 downto 0);
		SRAM_DQ: inout std_logic_vector(15 downto 0);
		SRAM_CE_N: out std_logic;
		SRAM_OE_N: out std_logic;
		SRAM_WE_N: out std_logic;
		SRAM_UB_N: out std_logic;
		SRAM_LB_N: out std_logic
	);
end full_HW5;


architecture arch_hw5 of full_HW5 is

	signal address: std_logic_vector(4 downto 0);
	
	signal input_data: std_logic_vector(7 downto 0);
	
	signal output_data: std_logic_vector(7 downto 0);
	signal we: std_logic;
		
	
	
	COMPONENT seven_led
	PORT(	
		vis_in:	in bit;
		bin_in:	in bit_vector (3 downto 0);
		hex_out:	out bit_vector (0 to 6)
		);
	END COMPONENT; 

begin

	we <= SW(17);
	
	
	address(4 downto 0) <= SW(15 downto 11);
	
	input_data <= SW(7 downto 0) when SW(17) = '1' else (others => 'Z');

	SRAM_WE_N <= (not we) or KEY(0); -- Asserted to 1 when KEY0 is not pressed
	
	SRAM_CE_N <= KEY(0); -- Asserted to 1 when KEY0 is not pressed
	SRAM_OE_N <= we or KEY(0); -- Asserted to 1 when KEY0 is not pressed
	SRAM_UB_N <= KEY(0); -- Asserted to 1 when KEY0 is not pressed
	SRAM_LB_N <= KEY(0); -- Asserted to 1 when KEY0 is not pressed
	
	SRAM_ADDR(19 downto 5) <= (others => '0');
	SRAM_ADDR(4 downto 0) <= address;
	
	SRAM_DQ(15 downto 8) <= (others => '0');
	SRAM_DQ(7 downto 0) <= input_data;
	
	output_data <= SRAM_DQ(7 downto 0);
	
	
	-- output
	
	LEDG(0) <= we; --write enable
	
	sled7: seven_led	PORT MAP( vis_in => '1', bin_in => to_bitvector("000" & address(4)), hex_out(0 to 6) => HEX7(0 to 6) ); --address
	sled6: seven_led	PORT MAP( vis_in => '1', bin_in => to_bitvector(address(3 downto 0)), hex_out(0 to 6) => HEX6(0 to 6) ); --address
	
	sled5: seven_led	PORT MAP( vis_in => to_bit(SW(17)) and to_bit(not KEY(0)), bin_in => to_bitvector(input_data(7 downto 4)), hex_out(0 to 6) => HEX5(0 to 6) ); --memory input
	sled4: seven_led	PORT MAP( vis_in => to_bit(SW(17)) and to_bit(not KEY(0)), bin_in => to_bitvector(input_data(3 downto 0)), hex_out(0 to 6) => HEX4(0 to 6) ); --memory input
	
	sled1: seven_led	PORT MAP( vis_in => to_bit(not SW(17)) and to_bit(not KEY(0)), bin_in => to_bitvector(output_data(7 downto 4)), hex_out(0 to 6) => HEX1(0 to 6) ); --memory output
	sled0: seven_led	PORT MAP( vis_in => to_bit(not SW(17)) and to_bit(not KEY(0)), bin_in => to_bitvector(output_data(3 downto 0)), hex_out(0 to 6) => HEX0(0 to 6) ); --memory output
	
	

end arch_hw5;