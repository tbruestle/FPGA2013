--Thomas Bruestle
--Greg Sleasman
--
--Sram.vhd
--
--This reads from the SRAM.
--The architecture of this file is quite interesting.
--We wrote this to accommodate multiple circuits inputting and outputting to SRAM.
--It operates working across several circuits, switching which one is used at 1 MHz.

LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;
USE  IEEE.sram_package.all;

entity sram is
	port(
		CLOCK_50  : in std_logic;
		new_write: in sram_write_array;
		new_address: in sram_address_type_array;
		new_input_data: in sram_data_type_array;
		new_enable: IN sram_enable_array;
		new_top_enable: IN sram_enable_array;
		new_bottom_enable: IN sram_enable_array;
		new_output_data: OUT sram_data_type_array;
		
		-- SRAM
		
		SRAM_ADDR: out std_logic_vector(19 downto 0);
		SRAM_DQ: inout std_logic_vector(15 downto 0);
		SRAM_CE_N: out std_logic;
		SRAM_OE_N: out std_logic;
		SRAM_WE_N: out std_logic;
		SRAM_UB_N: out std_logic;
		SRAM_LB_N: out std_logic
	);
end sram;


architecture arch_sram of sram is

	signal address: sram_address_type;
	
	signal input_data: sram_data_type;
	
	signal output_data: sram_data_type;
	signal we: std_logic;
	signal enab: std_logic;
		
	signal current_item: integer range sram_item_min - 1 to sram_item_max;
			-- use sram_item_min - 1 to avoid use on the reset
	
	signal clock_1MHz : STD_LOGIC;

	COMPONENT clk_div 
	PORT
	(
		clock_50Mhz			: IN	STD_LOGIC;
		clock_1MHz				: OUT	STD_LOGIC;
		clock_100KHz			: OUT	STD_LOGIC;
		clock_10KHz				: OUT	STD_LOGIC;
		clock_1KHz				: OUT	STD_LOGIC;
		clock_100Hz				: OUT	STD_LOGIC;
		clock_10Hz				: OUT	STD_LOGIC;
		clock_1Hz				: OUT	STD_LOGIC
	);
	END COMPONENT;
		
	COMPONENT seven_led
	PORT(	
		vis_in:	in bit;
		bin_in:	in bit_vector (3 downto 0);
		hex_out:	out bit_vector (0 to 6)
		);
	END COMPONENT; 

begin

	
		clk_div1 : clk_div port map
		(	
		  clock_50Mhz  => CLOCK_50,
		  clock_1MHz => clock_1MHz
		);
		
	  process(clock_1MHz)
		  variable step : integer range sram_item_min - 1 to sram_item_max;
			-- use sram_item_min - 1 to avoid use on the reset
	  begin
--			if (reset='1') then
--				step := sram_item_min - 1;
--			elsif ( clock_10KHz'event and clock_10KHz='1' ) then
			if ( clock_1MHz'event and clock_1MHz='1' ) then
				if (step = sram_item_max) then
					step := sram_item_min;
				else
					step := step + 1;
				end if;
			else
				step := step;
			end if;
			current_item <= step;
     end process;

	  process(CLOCK_50)
	  begin
--			if (reset='1') then
--				we <= '0';
--				enab <= '0';
--				address <= (others => 'Z');
--				input_data <= (others => 'Z');
--			elsif ( CLOCK_50'event and CLOCK_50='1' ) then
			if ( CLOCK_50'event and CLOCK_50='1' ) then
				we <= new_write(current_item);
				enab <= new_enable(current_item);
				
				address <= new_address(current_item);
				if new_write(current_item) = '1' then 
					input_data <= new_input_data(current_item);
				else
					input_data <= (others => 'Z');
				end if;
				
				
				--input_data <= new_input_data(0) when new_write(0) = '1' else (others => 'Z');
				
				
				--input_data <= new_input_data(0) when new_write(0) = '1' else (others => 'Z');

				
				new_output_data(current_item) <= output_data;
			end if;
     end process;
	-- output
				SRAM_DQ(15 downto 0) <= input_data;
				
				SRAM_WE_N <= (not we) or not enab; -- Asserted to 1 when KEY0 is not pressed
				
				SRAM_CE_N <= not enab; -- Asserted to 1 when KEY0 is not pressed
				SRAM_OE_N <= we or not enab; -- Asserted to 1 when KEY0 is not pressed
				SRAM_UB_N <= not enab; -- Asserted to 1 when KEY0 is not pressed
				SRAM_LB_N <= not enab; -- Asserted to 1 when KEY0 is not pressed
				
				SRAM_ADDR(19 downto 0) <= address;
				
				
				output_data <= SRAM_DQ(15 downto 0);
	

end arch_sram;