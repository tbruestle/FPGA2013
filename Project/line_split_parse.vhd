library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.FINAL_PACKAGE_PROJECT.all;
use ieee.numeric_std.all;

Entity line_split_parse is
	port(	
		clk						: IN	STD_LOGIC;
		reset						: IN	STD_LOGIC;
		
		--Connect to Line Read from File
		INPUT_LINE				: IN	INPUT_LINE_STRING;
		DONE_NEXT_LINE			: IN	STD_LOGIC;
		REQUEST_NEXT_LINE		: OUT	STD_LOGIC;
		
		
		--Connect to Word Parsing
		OUTPUT_WORD				: OUT SHORT_STRING;
		OUTPUT_WORD_LENGTH	: OUT integer range Stringmin to ShortStringmax;
		DONE_NEXT_WORD			: OUT	STD_LOGIC;
		REQUEST_NEXT_WORD		: IN	STD_LOGIC
	);
end line_split_parse;

architecture lsp of line_split_parse is
	signal shifted_input_line	: INPUT_LINE_STRING;
	
	
	signal to_shift	: INTEGER range Stringmin to Stringmax + 1; -- signals the output to shift where
	signal to_output	: STD_LOGIC;	--Signals the processor to output the current line
	
	signal word_shift	: INTEGER range Stringmin to Stringmax + 1; -- indicates how much from the start of INPUT_LINE the current word is
	signal word_length	: INTEGER range Stringmin to Stringmax + 1; -- indicates the length of the current word
	
	signal processing_word : STD_LOGIC;			--Mask of REQUEST_NEXT_WORD, indicates the next circuit is the word it just received
	signal done_processing_word : STD_LOGIC;  --Mask of DONE_NEXT_WORD, indicates the next circuit is the word it just received
	
	signal done_processing_line : STD_LOGIC; -- Indicates that the line is fully processed.
	
	signal in_text : STD_LOGIC; --indicates that text is currently being processed
	signal potential_comment : STD_LOGIC; --indicates that there could be a comment in the next character
begin

	DONE_NEXT_WORD <= done_processing_word;

	process(clk, reset)
	begin
		if (clk'event and clk='1') then 
			if reset='1' or DONE_NEXT_LINE='0' then -- reset
				shifted_input_line <= INPUT_LINE;
				
				to_shift <= 0;
				to_output <= '0';
				
				word_shift	<= 0;
				word_length	<= 0;
				
				processing_word <= '0';
				
				done_processing_word <= '0';
				done_processing_line <= '0';
				
				in_text	<= '0';
				
				potential_comment	<= '0';
				REQUEST_NEXT_LINE <= '0';
--			elsif done_processing_line = '1' then
--				done_processing_word <= '0'; -- will be replaced to wait for request_next_line
--				to_shift <= 0;
--				to_output <= '0';
--				
--				word_shift	<= 0;
--				word_length	<= 0;
--				processing_word <= '0';
--				in_text	<= '0';
--				
--				potential_comment	<= '0';
			elsif processing_word='0' and done_processing_line = '0' then
				if to_output = '1' then
					in_text <= '1';
					OUTPUT_WORD(Stringmin to ShortStringmax) <= INPUT_LINE(Stringmin to ShortStringmax);
					OUTPUT_WORD_LENGTH <= word_length;
					REQUEST_NEXT_LINE <= '0';
					to_shift <= word_length;
					to_output <= '0';
					done_processing_word <= '1';
					if word_shift + word_length >= Stringmax then
						done_processing_line <= '1';	
					end if;
				elsif to_shift > 0 then  -- Shift characters
					if word_shift + to_shift >= Stringmax then
						done_processing_line <= '1';
					else
						word_shift	<= word_shift + 1;
						shifted_input_line(Stringmin to Stringmax) <= shifted_input_line(Stringmin + 1 to Stringmax) & '-';
						to_shift <= to_shift - 1;
					end if;
				elsif word_shift + word_length >= Stringmax then --
					if in_text = '1' then
						to_output <= '1';	
					else
						to_shift <= word_length;
					end if;
				elsif potential_comment = '1' then
					if shifted_input_line(word_length)='-' then
						potential_comment <= '0';
						word_length <= word_length + 1;
						done_processing_line <= '1';
					else
						to_output <= '1';
						OUTPUT_WORD(0 to 0) <= "-";
					end if;
				else
					case INPUT_LINE(word_length) is
						when '-' => 
							if in_text = '1' then
								to_output <= '1';	
							else
								potential_comment <= '1';
								word_length <= word_length + 1;
							end if;
--						when '\\' => 
--							in_text <= '1';
--							current_word_location <= current_word_location + 2;
						when ' ' => 
							if in_text='1' then
								to_output <= '1';	
							else
								to_shift <= 1;
							end if;
						when ';'|'/' =>
							if in_text='1' then
								to_output <= '1';	
							else
								to_output <= '1';
								word_length <= word_length + 1;
							end if;
--						when '\''|"\""|"/"|":"|"<"|">"|"("|")" =>
--							OUTPUT_WORD <= INPUT_LINE(current_word_location);
--							processing_word <= '0';
--							done_processing_word <= '1';
--							current_word_location <= current_word_location + 1;
						when others =>
							in_text <= '1';
							word_length <= word_length + 1;
		--			  when 	"a","b","c","d","e","f","g","h","i","j","k","l","m",
		--						"n","o","p","q","r","s","t","u","v","w","x","y","z",
		--						"A","B","C","D","E","F","G","H","I","J","K","L","M",
		--						"N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
		--						"0","1","2","3","4","5","6","7","8","9","_"
					end case;
				end if;
			end if;
		end if;
	end process;

	
	
	
end lsp;
