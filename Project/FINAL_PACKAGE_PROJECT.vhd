library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

package FINAL_PACKAGE_PROJECT is

	constant Xmin : integer := 0;
	constant Xmax : integer := 499;
	constant Ymin : integer := 0;
	constant Ymax : integer := 499;

	constant Stringmin : integer := 0;
	constant ShortStringmax : integer := 63;
	constant Stringmax : integer := 255;
	
	type CHAR_ARRAY is array (integer range <>) of character;
	
	subtype SHORT_STRING is CHAR_ARRAY (Stringmin to ShortStringmax);
	subtype INPUT_LINE_STRING is CHAR_ARRAY (Stringmin to Stringmax);
	
	
	type XYPoint is									--	A point on the image
	record
		X : INTEGER range Xmin to Xmax; 			-- The X position of a Point
		Y : INTEGER range Ymin to Ymax; 			-- The Y position of a Point
	end record;

	type Color is										--	A color of a point on the image
	record
		R : INTEGER range 0 to 255; 				-- The Red amount the color
		G : INTEGER range 0 to 255; 				-- The Green amount the color
		B : INTEGER range 0 to 255; 				-- The Blue amount the color
	end record;

	type EntityWire is								--	Data containing a wire on the entity
	record
		TEXT_SIZE  : INTEGER range 0 to 255; 	-- The size of the variable name
		TEXT_VALUE  : SHORT_STRING;				-- The variable name
		IS_INPUT  : std_logic_vector(1 downto 0); 						-- 2= In/out, 1 = Input, 0 = Output
		IS_BUS  : std_logic; 						-- 1 = Bus (Thick wire), 0 = wire (Thin Wire)
	end record;

	type EntityTitle is								--	Data containing a title on the entity
	record
		TEXT_SIZE  : INTEGER range 0 to 255; 	-- The size of the title
		TEXT_VALUE  : SHORT_STRING; 						-- The title
	end record;

	type TextAtLocation is							--	Add text at location on the image
	record
		TEXT_SIZE  : INTEGER range 0 to 255; 	-- The size of the text
		TEXT_VALUE  : SHORT_STRING; 						-- The text
		TOPLEFT : XYPoint; 							-- The top left location of the text
		TEXT_COLOR : Color;							-- The color of the text
	end record;

	type LineAtLocation is							--	Add a line on the image
	record
		POINT1 : XYPoint; 							-- The first point in a line
		POINT2 : XYPoint; 							-- The second point in a line
		LINE_COLOR : Color;							-- The color of the line
	end record;
	
	type ImageType is array (						--	An image
		Xmin to Xmax,									-- The width
		Ymin to Ymax									-- The height
	) of Color;
end FINAL_PACKAGE_PROJECT;