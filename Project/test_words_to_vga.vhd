--Thomas Bruestle
--Greg Sleasman
--
--Test_words_to_vga.vhd (Top level entity) 
--
--This will write a set of predetermined strings to VGA as inputs and outputs.
--While this works similar to Vga.vhd, it tests the circuit vga2.vhd used in the main program,
--and vga2_wrapper.vhd, used in sram_to_vga_test.vhd. 


  library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.STD_LOGIC_UNSIGNED.ALL;
  use IEEE.STD_LOGIC_ARITH.ALL;
  use ieee.numeric_std.all;
  use IEEE.vga_package.all;
 
	
  entity test_words_to_vga is
    port(CLOCK_50  : in std_logic;
	      SW  : in std_logic_vector(17 downto 0);
			VGA_R   : out std_logic_vector(7 downto 0);
			VGA_G : out std_logic_vector(7 downto 0);
         VGA_B  : out std_logic_vector(7 downto 0);
         VGA_HS    : out std_logic;
         VGA_VS    : out std_logic;
			VGA_SYNC_N      : out std_logic;
			VGA_BLANK_N     : out std_logic;
			VGA_CLK	 : out std_logic);
  end test_words_to_vga;
 
  architecture Behavioral_test_words_to_vga of test_words_to_vga is
  
  COMPONENT vga2_wrapper
  PORT(	CLOCK_50  : in std_logic;
	      new_input  : in ARRAY_OF_STRINGS (1 to 6);
	      new_output: in ARRAY_OF_STRINGS (1 to 6);
	      SW  : in std_logic_vector(17 downto 0);
			VGA_R   : out std_logic_vector(7 downto 0);
			VGA_G : out std_logic_vector(7 downto 0);
         VGA_B  : out std_logic_vector(7 downto 0);
         VGA_HS    : out std_logic;
         VGA_VS    : out std_logic;
			VGA_SYNC_N      : out std_logic;
			VGA_BLANK_N     : out std_logic;
			VGA_CLK	 : out std_logic);
	END COMPONENT;
	
	  signal new_input : ARRAY_OF_STRINGS (1 to 6);
	  signal new_output : ARRAY_OF_STRINGS (1 to 6);
  begin
  
		  new_input(1) <= "ABCDEFGH";new_output(1) <= "LMNOPQRS";
		  new_input(2) <= "IJKLMNOP";new_output(2) <= "TUVWXYZ0";
		  new_input(3) <= "QRSTUVWX";new_output(3) <= "12345678";
		  new_input(4) <= "YZ012345";new_output(4) <= "9_ABCDEF";
		  new_input(5) <= "6789_ABC";new_output(5) <= "GHIJKLMN";
		  new_input(6) <= "DEFGHIJK";new_output(6) <= "OPQRSTUV";
  
  
      vga2_wrapperX : vga2_wrapper port map
			(	
			  CLOCK_50  => CLOCK_50,
			  new_input => new_input,
			  new_output => new_output,
			  SW => SW,
			  VGA_R => VGA_R,
			  VGA_G => VGA_G,
			  VGA_B => VGA_B,
			  VGA_HS => VGA_HS,
			  VGA_VS => VGA_VS,
			  VGA_SYNC_N => VGA_SYNC_N,
			  VGA_BLANK_N => VGA_BLANK_N,
			  VGA_CLK => VGA_CLK
		   );
  end Behavioral_test_words_to_vga;