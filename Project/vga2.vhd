--Thomas Bruestle
--Greg Sleasman
--
--Vga2.vhd
--
--This is the output to the VHD. The reset signal will clear the inputs.
-- When a new character is added at either an input or an output,
-- the number of inputs and outputs increase to accommodate the most recent input.

  library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.STD_LOGIC_UNSIGNED.ALL;
  use IEEE.STD_LOGIC_ARITH.ALL;
  use ieee.numeric_std.all;
  use IEEE.vga_package.all;
 
	
  entity vga2 is
    port(CLOCK_50  : in std_logic;
			--write inputs
	      reset  : in std_logic; -- reset on 1
	      new_character  : in character;
	      new_character_letter_num : in integer range letter_num_min to letter_num_max;
	      new_character_io_num : in integer range 1 to 6;
			new_io : in std_logic; -- 1 is output, 0 to input
			new_write : in std_logic; -- only write on 1
			--VGA
			VGA_R   : out std_logic_vector(7 downto 0);
			VGA_G : out std_logic_vector(7 downto 0);
         VGA_B  : out std_logic_vector(7 downto 0);
         VGA_HS    : out std_logic;
         VGA_VS    : out std_logic;
			VGA_SYNC_N      : out std_logic;
			VGA_BLANK_N     : out std_logic;
			VGA_CLK	 : out std_logic
			);
  end vga2;
 
  architecture Behavioral2 of vga2 is
	  signal clk25,vs_out,hs_out : std_logic;
	  signal horizontal_counter : std_logic_vector(9 downto 0);
	  signal vertical_counter : std_logic_vector(9 downto 0);
     signal input_count  : integer range 0 to 6;
	  signal output_count  : integer range 0 to 6;
	  signal input : ARRAY_OF_STRINGS (1 to 6);
	  signal output : ARRAY_OF_STRINGS (1 to 6);
	
	COMPONENT  vga_word_parse
	PORT
	(
		letters  : in EIGHT_STRING;
	   blocks  : out LETTER_BLOCK_ARRAY
	);
	END COMPONENT;
	
	
	
	  --THE FOLLOWING 2 LINES ARE ONLY USED FOR TESTING, THEY BECOME INPUTS IN REAL LIFE
--	  signal input : ARRAY_OF_STRINGS (1 to 6);
--	  signal output : ARRAY_OF_STRINGS (1 to 6);
	  
	  
	  
	  
	  
	  --INPUT TEXT BLOCKS
	  signal blockin : ARRAY_OF_BLOCKS (1 to 6);
	  --OUTPUT TEXT BLOCKS
	  signal blockout : ARRAY_OF_BLOCKS (1 to 6);
	  
	  signal in_input : integer range -1 to input_num_max;
	  signal in_output : integer range -1 to output_num_max;
	  signal in_character : integer range -1 to letter_num_max;
	  signal in_character_left : integer range -1 to letter_right;
	  signal in_character_top : integer range -1 to letter_bottom;
  begin
  
  
 process (CLOCK_50)
 begin
	if (reset='1') then
		  input_count <= 0;  --latch
		  output_count <= 0;  --latch
		  input(1) <= "        ";output(1) <= "        ";
		  input(2) <= "        ";output(2) <= "        ";
		  input(3) <= "        ";output(3) <= "        ";
		  input(4) <= "        ";output(4) <= "        ";
		  input(5) <= "        ";output(5) <= "        ";
		  input(6) <= "        ";output(6) <= "        ";
	elsif CLOCK_50'event and CLOCK_50='1' then --on CLOCK_50 rather than new_write to ensure constant write
		if (new_write='1' and new_io='0') then
			input(new_character_io_num)(new_character_letter_num) <= new_character;
			if (input_count < new_character_io_num) then
				input_count <=  new_character_io_num; 
			end if;
		elsif (new_write='1' and new_io='1') then
			output(new_character_io_num)(new_character_letter_num) <= new_character;
			if (output_count < new_character_io_num) then
				output_count <=  new_character_io_num; 
			end if;
		end if;
	end if;
 end process;
  
  


   GEN_vga_word_parse_in: 
   for I in input_num_min to input_num_max  generate
      vga_word_parse_inX : vga_word_parse port map
			(	letters => input(I), blocks => blockin(I) );
   end generate GEN_vga_word_parse_in;
	
   GEN_vga_word_parse_out: 
   for I in output_num_min to output_num_max  generate
      vga_word_parse_outX : vga_word_parse port map
			(	letters => output(I), blocks => blockout(I) );
   end generate GEN_vga_word_parse_out;
 
  
 
 process (horizontal_counter, vertical_counter)
 
    VARIABLE height_div: INTEGER RANGE 0 TO output_num_max - output_num_min := 0;
    VARIABLE input_left_div: INTEGER RANGE 0 TO  input_num_max - input_num_min := 0;
    VARIABLE output_left_div: INTEGER RANGE 0 TO letter_num_max - letter_num_min := 0;
    VARIABLE height_mod: INTEGER RANGE 0 TO letter_height_between - 1 := 0;
    VARIABLE input_left_mod: INTEGER RANGE 0 TO letter_width_between - 1 := 0;
    VARIABLE output_left_mod: INTEGER RANGE 0 TO letter_width_between - 1 := 0;
	 
	 
 
    CONSTANT input_draw_left : integer := inleft + letter_left;
    CONSTANT input_draw_right : integer := inleft + ((letter_num_max - letter_num_min) * letter_width_between) + letter_right;
	 
    CONSTANT output_draw_left : integer := outleft + letter_left;
    CONSTANT output_draw_right : integer := outleft + ((letter_num_max - letter_num_min) * letter_width_between) + letter_right;
	 
    CONSTANT letter_draw_top : integer := letters_top + letter_top;
    CONSTANT letter_draw_bottom : integer := letters_top + ((input_num_max - input_num_min) * letter_height_between) + letter_bottom;
	 
	 
	 
 begin
	height_div := (conv_integer(vertical_counter) - letters_top) / letter_height_between;
	input_left_div := (conv_integer(horizontal_counter) - inleft) / letter_width_between;
	output_left_div := (conv_integer(horizontal_counter) - outleft) / letter_width_between;
	
	height_mod := (conv_integer(vertical_counter) - letters_top) MOD letter_height_between;
	input_left_mod := (conv_integer(horizontal_counter) - inleft) MOD letter_width_between;
	output_left_mod := (conv_integer(horizontal_counter) - outleft) MOD letter_width_between;

	
	if (
	(conv_integer(horizontal_counter) >= input_draw_left) and
	(conv_integer(horizontal_counter) <= input_draw_right) and
	(conv_integer(vertical_counter) >= letter_draw_top) and
	(conv_integer(vertical_counter) <= letter_draw_bottom) 
	) then
	
	--May be in an input
		in_output <= -1;
		if (
		( input_left_mod > letter_right ) or
		( input_left_mod < letter_left ) or
		( height_mod < letter_top ) or
		( height_mod > letter_bottom )
			) then
			
			--not input
			in_input <= -1;
			in_character <= -1;
			in_character_left <= -1;
			in_character_top <= -1;
			
		else
			
			in_input <= height_div + input_num_min;
			in_character <= input_left_div + letter_num_min;
			in_character_left <= input_left_mod;
			in_character_top <= height_mod;
			
		end if;
		
	elsif (
	(conv_integer(horizontal_counter) >= output_draw_left) and
	(conv_integer(horizontal_counter) <= output_draw_right) and
	(conv_integer(vertical_counter) >= letter_draw_top) and
	(conv_integer(vertical_counter) <= letter_draw_bottom) 
	) then
	
	--May be in an input
		in_input <= -1;
		if (
		( output_left_mod > letter_right ) or
		( output_left_mod < letter_left ) or
		( height_mod < letter_top ) or
		( height_mod > letter_bottom )
			) then
			
			--not input
			in_output <= -1;
			in_character <= -1;
			in_character_left <= -1;
			in_character_top <= -1;
			
		else
			
			in_output <= height_div + output_num_min;
			in_character <= output_left_div + letter_num_min;
			in_character_left <= output_left_mod;
			in_character_top <= height_mod;
			
		end if;
		
	else
		in_input <= -1;
		in_output <= -1;
		in_character <= -1;
		in_character_left <= -1;
		in_character_top <= -1;
	
	end if;
	
 
 end process;
 
 
  process (CLOCK_50)
  begin
    if CLOCK_50'event and CLOCK_50='1' then
      if (clk25 = '0') then
        clk25 <= '1';
      else
        clk25 <= '0';
      end if;
    end if;
  end process;
 
  
  process (clk25)
  begin
    if ( clk25'event and clk25 = '1' ) then				-- USE RANGE OF 144 - 784 for H 
		
		if ((((horizontal_counter >= 374 ) 						-- USE RANGE OF 35 - 515 for V
      and (horizontal_counter < 384 )) or ((horizontal_counter >= 524 ) 					
      and (horizontal_counter < 534 )))
      and (vertical_counter >= 135 ) 
      and (vertical_counter < 425 )) 
      then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
		 
		elsif (((horizontal_counter >= 374 ) 						
      and (horizontal_counter < 534 )) 
      and (((vertical_counter >= 135 ) 
      and (vertical_counter < 145 )) or ((vertical_counter >= 415 ) 
      and (vertical_counter < 425 ))))
      then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 334 ) 						
		and (horizontal_counter < 374 ) 
		and (vertical_counter >= 155 ) 
		and (vertical_counter < 158 ) and ( input_count > 0 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
		 
		elsif ((horizontal_counter >= 334 ) 						
		and (horizontal_counter < 374 ) 
		and (vertical_counter >= 188 ) 
		and (vertical_counter < 191 ) and ( input_count > 1 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 334 ) 						
		and (horizontal_counter < 374 ) 
		and (vertical_counter >= 221) 
		and (vertical_counter < 224) and ( input_count > 2 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 334 ) 						
		and (horizontal_counter < 374 ) 
		and (vertical_counter >= 254 ) 
		and (vertical_counter < 257 ) and ( input_count > 3 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 334 ) 						
		and (horizontal_counter < 374 ) 
		and (vertical_counter >= 287 ) 
		and (vertical_counter < 290 ) and ( input_count > 4 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 334 ) 						
		and (horizontal_counter < 374 ) 
		and (vertical_counter >= 320 ) 
		and (vertical_counter < 323 ) and ( input_count > 5 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
--		elsif ((horizontal_counter >= 334 ) 						
--		and (horizontal_counter < 374 ) 
--		and (vertical_counter >= 353 ) 
--		and (vertical_counter < 356 ) and ( input_count > 6 ))
--		then
--			VGA_R <= "11111111";
--			VGA_G <= "11111111";
--			VGA_B <= "11111111";
			
--		elsif ((horizontal_counter >= 334 ) 						
--		and (horizontal_counter < 374 ) 
--		and (vertical_counter >= 386 ) 
--		and (vertical_counter < 389 ) and ( input_count > 7 ))
--		then
--			VGA_R <= "11111111";
--			VGA_G <= "11111111";
--			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 534 ) 						
		and (horizontal_counter < 574 ) 
		and (vertical_counter >= 155 ) 
		and (vertical_counter < 158 ) and ( output_count > 0 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 534 ) 						
		and (horizontal_counter < 574 ) 
		and (vertical_counter >= 188 ) 
		and (vertical_counter < 191 ) and ( output_count > 1 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 534 ) 						
		and (horizontal_counter < 574 ) 
		and (vertical_counter >= 221) 
		and (vertical_counter < 224) and ( output_count > 2 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 534 ) 						
		and (horizontal_counter < 574 ) 
		and (vertical_counter >= 254 ) 
		and (vertical_counter < 257 ) and ( output_count > 3 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 534 ) 						
		and (horizontal_counter < 574 ) 
		and (vertical_counter >= 287 ) 
		and (vertical_counter < 290 ) and ( output_count > 4 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
		elsif ((horizontal_counter >= 534 ) 						
		and (horizontal_counter < 574 ) 
		and (vertical_counter >= 320 ) 
		and (vertical_counter < 323 ) and ( output_count > 5 ))
		then
			VGA_R <= "11111111";
			VGA_G <= "11111111";
			VGA_B <= "11111111";
			
--		elsif ((horizontal_counter >= 534 ) 						
--		and (horizontal_counter < 574 ) 
--		and (vertical_counter >= 353 ) 
--		and (vertical_counter < 356 ) and ( output_count > 6 ))
--		then
--			VGA_R <= "11111111";
--			VGA_G <= "11111111";
--			VGA_B <= "11111111";
--			
--		elsif ((horizontal_counter >= 534 ) 						
--		and (horizontal_counter < 574 ) 
--		and (vertical_counter >= 386 ) 
--		and (vertical_counter < 389 ) and ( output_count > 7 ))
--		then
--			VGA_R <= "11111111";
--			VGA_G <= "11111111";
--			VGA_B <= "11111111";
--			
		elsif (
		(in_input > -1) 
		and (in_character > -1) 
		and (in_character_left > -1)  
		and (in_character_top > -1)) 
		then
			if blockin(in_input)(in_character)(in_character_top)(in_character_left)='1' then
				VGA_R <= "11111111";
				VGA_G <= "11111111";
				VGA_B <= "11111111";
			else
				VGA_R <= "00000000";
				VGA_G <= "00000000";
				VGA_B <= "00000000";
			end if;
		elsif (
		(in_output > -1) and
		(in_character > -1) and
		(in_character_left > -1) and 
		(in_character_top > -1))
		then
			if blockout(in_output)(in_character)(in_character_top)(in_character_left)='1' then
				VGA_R <= "11111111";
				VGA_G <= "11111111";
				VGA_B <= "11111111";
			else
				VGA_R <= "00000000";
				VGA_G <= "00000000";
				VGA_B <= "00000000";
			end if;
		else
			VGA_R <= "00000000";
			VGA_G <= "00000000";
			VGA_B <= "00000000";
      end if;
      
		if (horizontal_counter > 0 )
        and (horizontal_counter < 97 ) 
      then
        hs_out <= '0';
      else
        hs_out <= '1';
      end if;
      
		if (vertical_counter > 0 )
        and (vertical_counter < 3 ) 
      
		then
        vs_out <= '0';
      else
        vs_out <= '1';
      end if;
      
		horizontal_counter <= horizontal_counter+1;
      
		if (horizontal_counter=screen_right+1) then
        vertical_counter <= vertical_counter+1;
        horizontal_counter <= "0000000000";
      end if;
      
		if (vertical_counter=screen_bottom+1) then
        vertical_counter <= "0000000000";
      
		end if;
   
	end if;
  
  end process;
 
  VGA_HS <= (not hs_out);
  VGA_VS <= (not vs_out); 
  VGA_SYNC_N <= '1';
  VGA_BLANK_N <= '1';
  VGA_CLK <= clk25;
  end Behavioral2;