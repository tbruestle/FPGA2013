--Thomas Bruestle
--Greg Sleasman
--
--Vga2_wrapper.vhd 
--
--This takes 6 input and output strings and sends them to Vga2.vhd.
--This is not used in the final project as it requires too many output pins.


  library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.STD_LOGIC_UNSIGNED.ALL;
  use IEEE.STD_LOGIC_ARITH.ALL;
  use ieee.numeric_std.all;
  use IEEE.vga_package.all;
 
	
  entity vga2_wrapper is
    port(CLOCK_50  : in std_logic;
	      new_input  : in ARRAY_OF_STRINGS (1 to 6);
	      new_output: in ARRAY_OF_STRINGS (1 to 6);
	      SW  : in std_logic_vector(17 downto 0);
			VGA_R   : out std_logic_vector(7 downto 0);
			VGA_G : out std_logic_vector(7 downto 0);
         VGA_B  : out std_logic_vector(7 downto 0);
         VGA_HS    : out std_logic;
         VGA_VS    : out std_logic;
			VGA_SYNC_N      : out std_logic;
			VGA_BLANK_N     : out std_logic;
			VGA_CLK	 : out std_logic);
  end vga2_wrapper;
 
  architecture Behavioral_vga2_wrapper of vga2_wrapper is
  
	
  COMPONENT vga2 
  PORT(
			CLOCK_50  : in std_logic;
			--write inputs
	      reset  : in std_logic; -- reset on 1
	      new_character  : in character;
	      new_character_letter_num : in integer range letter_num_min to letter_num_max;
	      new_character_io_num : in integer range 1 to 6;
			new_io : in std_logic; -- 1 is output, 0 to input
			new_write : in std_logic; -- only write on 1
			--VGA
			VGA_R   : out std_logic_vector(7 downto 0);
			VGA_G : out std_logic_vector(7 downto 0);
         VGA_B  : out std_logic_vector(7 downto 0);
         VGA_HS    : out std_logic;
         VGA_VS    : out std_logic;
			VGA_SYNC_N      : out std_logic;
			VGA_BLANK_N     : out std_logic;
			VGA_CLK	 : out std_logic
	);
	END COMPONENT;
 
	COMPONENT clk_div 
	PORT
	(
		clock_50Mhz			: IN	STD_LOGIC;
		clock_1MHz				: OUT	STD_LOGIC;
		clock_100KHz			: OUT	STD_LOGIC;
		clock_10KHz				: OUT	STD_LOGIC;
		clock_1KHz				: OUT	STD_LOGIC;
		clock_100Hz				: OUT	STD_LOGIC;
		clock_10Hz				: OUT	STD_LOGIC;
		clock_1Hz				: OUT	STD_LOGIC
	);
	END COMPONENT;
	
	  signal input_count, output_count : std_logic_vector(2 downto 0);
	  signal input : ARRAY_OF_STRINGS (1 to 6);
	  signal output : ARRAY_OF_STRINGS (1 to 6);
	  signal clock_1KHz : STD_LOGIC;
	  
	  signal new_character  : character;
	  signal new_character_letter_num : integer range letter_num_min to letter_num_max;
	  signal new_character_io_num : integer range 1 to 6;
	  signal new_io : std_logic; -- 1 is output, 0 to input
	  signal new_write : std_logic; -- only write on 1
  begin
  
		clk_div1 : clk_div port map
		(	
		  clock_50Mhz  => CLOCK_50,
		  clock_1KHz => clock_1KHz
		);
	  process(SW(17), clock_1KHz)
		  variable step : integer range 0 to 16 * letter_num_range;
		  variable new_io_number : integer range 0 to 16 ;
		  variable new_io_letter : integer range 0 to letter_num_max;
	  begin
		new_io_number := (step / letter_num_range);
		new_io_letter := (step mod letter_num_range);
		if (SW(17)='1') then
			new_write <= '0';
			step := 0;
		elsif (new_io_number >= 12) then
			new_write <= '0';
			step := 0;
		elsif ( clock_1KHz'event and clock_1KHz='1' ) then
			new_write <= '1';
			new_character_letter_num <= new_io_letter + letter_num_min;
			if (new_io_number < 6) then
				new_io <= '0';
				new_character_io_num <= new_io_number + 1;
				new_character <= new_input(new_io_number + 1)(new_io_letter + letter_num_min);
			else
				new_io <= '1';
				new_character_io_num <= new_io_number - 5;
				new_character <= new_output(new_io_number - 5)(new_io_letter + letter_num_min);
			end if;
			step := step + 1;
		end if;
	  end process;
  
      vga2X : vga2 port map
			(	
			  CLOCK_50  => CLOCK_50,
			  reset => SW(17),
			  new_character => new_character,
			  new_character_letter_num => new_character_letter_num,
			  new_character_io_num => new_character_io_num,
			  new_io => new_io,
			  new_write => new_write,
			  VGA_R => VGA_R,
			  VGA_G => VGA_G,
			  VGA_B => VGA_B,
			  VGA_HS => VGA_HS,
			  VGA_VS => VGA_VS,
			  VGA_SYNC_N => VGA_SYNC_N,
			  VGA_BLANK_N => VGA_BLANK_N,
			  VGA_CLK => VGA_CLK
		   );
  
  end Behavioral_vga2_wrapper;