
--FinalProject.vhd (Top level entity)
--
--This was the original structure of the layout of the top level entity as included in the proposal.
--We diverged greatly from this, most importantly because we had to use SRAM.


library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.FINAL_PACKAGE_PROJECT.all;

Entity FinalProject is
	port(	
		reset : in std_logic;					-- A bit indicating to clear the entire circuit	
		clock_50Mhz : in std_logic;			-- A 50 mHz clock input
	
		--Connect to Line Read from File
		INPUT_LINE :	in INPUT_LINE_STRING;-- A single line of input
		DONE_NEXT_LINE : in std_logic;		-- A signal indicating the Input Line is ready for processing
		REQUEST_NEXT_LINE : out std_logic;	-- A signal indicating the circuit is ready for the next Input Line
		
		
		--Connect to Image Output
		IMAGE_OUT : out ImageType				-- The output image
	);
end FinalProject;

architecture fp of FinalProject is

	COMPONENT  clk_div
	PORT
	(
		clock_50Mhz			: IN	STD_LOGIC;
		clock_1MHz			: OUT	STD_LOGIC;
		clock_100KHz		: OUT	STD_LOGIC;
		clock_10KHz			: OUT	STD_LOGIC;
		clock_1KHz			: OUT	STD_LOGIC;
		clock_100Hz			: OUT	STD_LOGIC;
		clock_10Hz			: OUT	STD_LOGIC;
		clock_1Hz			: OUT	STD_LOGIC
	);
	END COMPONENT;

	COMPONENT  parse_text
	PORT
	(
		clk						: IN	STD_LOGIC;
		reset						: IN	STD_LOGIC;
		
		--Connect to Line Read from File
		INPUT_LINE				: IN	INPUT_LINE_STRING;
		DONE_NEXT_LINE			: IN	STD_LOGIC;
		REQUEST_NEXT_LINE		: OUT	STD_LOGIC;
		
		
		--Connect to variable_title_parse
		TITLE_VALUE					: OUT	EntityTitle;
		VARIABLE_VALUE				: OUT	EntityWire;
		WRITE_TITLE					: OUT	STD_LOGIC;
		WRITE_VARIABLE				: OUT	STD_LOGIC;
		REQUEST_TITLE_VARIABLE	: IN	STD_LOGIC
	);
	END COMPONENT;
	
	

	COMPONENT  variable_title_parse
	PORT
	(
		clk						: IN	STD_LOGIC;
		reset						: IN	STD_LOGIC;
		
		--Connect to parse_text
		TITLE_VALUE					: IN	EntityTitle;
		VARIABLE_VALUE				: IN	EntityWire;
		WRITE_TITLE					: IN	STD_LOGIC;
		WRITE_VARIABLE				: IN	STD_LOGIC;
		REQUEST_TITLE_VARIABLE	: OUT	STD_LOGIC;
		
		
		--Connect to image_write_parse
		WRITE_TEXT_VALUE			: OUT	TextAtLocation;
		WRITE_LINE_VALUE			: OUT	LineAtLocation;
		DONE_TEXT					: OUT	STD_LOGIC;
		DONE_LINE					: OUT	STD_LOGIC;
		DONE_ERASE_IMAGE			: OUT	STD_LOGIC;
		REQUEST_IMAGE_WRITE		: IN	STD_LOGIC
	);
	END COMPONENT;
	
	
	COMPONENT  image_write_parse
	PORT
	(
		clk						: IN	STD_LOGIC;
		reset						: IN	STD_LOGIC;
		
		--Connect to variable_title_parse
		WRITE_TEXT_VALUE			: IN	TextAtLocation;
		WRITE_LINE_VALUE			: IN	LineAtLocation;
		DONE_TEXT					: IN	STD_LOGIC;
		DONE_LINE					: IN	STD_LOGIC;
		DONE_ERASE_IMAGE			: IN	STD_LOGIC;
		REQUEST_IMAGE_WRITE		: OUT	STD_LOGIC;
		
		
		--Connect to image_write_parse
		IMAGE_OUT 					: OUT	ImageType
	);
	END COMPONENT;
	
	signal clock_1MHz		:	STD_LOGIC;
	signal clock_100KHz	:	STD_LOGIC;
	signal clock_10KHz	:	STD_LOGIC;
	signal clock_1KHz		:	STD_LOGIC;
	signal clock_100Hz	: STD_LOGIC;
	signal clock_10Hz		: STD_LOGIC;
	signal clock_1Hz		:	STD_LOGIC;
	
	signal TITLE_VALUE				: EntityTitle;
	signal VARIABLE_VALUE			: EntityWire;
	signal WRITE_TITLE				: STD_LOGIC;
	signal WRITE_VARIABLE			: STD_LOGIC;
	signal REQUEST_TITLE_VARIABLE	: STD_LOGIC;
	
	signal WRITE_TEXT_VALUE			: TextAtLocation;
	signal WRITE_LINE_VALUE			: LineAtLocation;
	signal DONE_TEXT					: STD_LOGIC;
	signal DONE_LINE					: STD_LOGIC;
	signal DONE_ERASE_IMAGE			: STD_LOGIC;
	signal REQUEST_IMAGE_WRITE		: STD_LOGIC;
begin

	clk1 : clk_div PORT MAP
	(
		clock_50Mhz => clock_50Mhz,
		clock_1MHz => clock_1MHz,
		clock_100KHz => clock_100KHz,
		clock_10KHz => clock_10KHz,
		clock_1KHz => clock_1KHz,
		clock_100Hz => clock_100Hz,
		clock_10Hz => clock_10Hz,
		clock_1Hz => clock_1Hz
	);

	
	
	pt1:  parse_text PORT MAP
	(
		clk => clock_1KHz,
		reset => reset,
		
		--Connect to Line Read from File
		INPUT_LINE => INPUT_LINE,
		DONE_NEXT_LINE => DONE_NEXT_LINE,
		REQUEST_NEXT_LINE => REQUEST_NEXT_LINE,
		
		--Connect to variable_title_parse
		TITLE_VALUE => TITLE_VALUE,
		VARIABLE_VALUE => VARIABLE_VALUE,
		WRITE_TITLE => WRITE_TITLE,
		WRITE_VARIABLE => WRITE_VARIABLE,
		REQUEST_TITLE_VARIABLE => REQUEST_TITLE_VARIABLE
	);
	
	

	vt1: variable_title_parse PORT MAP
	(
		clk => clock_1KHz,
		reset => reset,
		
		--Connect to parse_text
		TITLE_VALUE => TITLE_VALUE,
		VARIABLE_VALUE => VARIABLE_VALUE,
		WRITE_TITLE => WRITE_TITLE,
		WRITE_VARIABLE => WRITE_VARIABLE,
		REQUEST_TITLE_VARIABLE => REQUEST_TITLE_VARIABLE,
		
		--Connect to image_write_parse
		WRITE_TEXT_VALUE => WRITE_TEXT_VALUE,
		WRITE_LINE_VALUE => WRITE_LINE_VALUE,
		DONE_TEXT => DONE_TEXT,
		DONE_LINE => DONE_LINE,
		DONE_ERASE_IMAGE => DONE_ERASE_IMAGE,
		REQUEST_IMAGE_WRITE => REQUEST_IMAGE_WRITE
	);
	
	
	
	iw1:  image_write_parse PORT MAP
	(
		clk => clock_1KHz,
		reset => reset,
		
		--Connect to variable_title_parse
		WRITE_TEXT_VALUE => WRITE_TEXT_VALUE,
		WRITE_LINE_VALUE => WRITE_LINE_VALUE,
		DONE_TEXT => DONE_TEXT,
		DONE_LINE => DONE_LINE,
		DONE_ERASE_IMAGE => DONE_ERASE_IMAGE,
		REQUEST_IMAGE_WRITE => REQUEST_IMAGE_WRITE,
		
		
		--Connect to image_write_parse
		IMAGE_OUT => IMAGE_OUT
	);
	
	
	
end fp;
