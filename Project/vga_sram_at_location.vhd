--Thomas Bruestle
--Greg Sleasman
--
--Vga_sram_at_location.vhd (Top level entity)
--
--This is the main program. As we were unable to complete the proposal, this is our submission.
--
--The program takes an address and a string length and adds the text at that address as an input or an output.
--
--The vhdl file is read into SRAM using the control panel.
--Using Sram.vhd, the contents of the SRAM are loaded character by character into the circuit.
--Next, each character is added using Vga2.vhd as either a character in the input or a character in the output.
--
--Input:
--
--SW(17) ? This is the reset signal. When this is set to 1, all inputs and outputs are cleared out.
--SW(16 downto 14) ? This is used to set the number of characters to read. As the range is 1 to 8,
--the mapping takes the binary number and adds 1 to it.
--SW(13 downto 0) ? This is the binary representation of the location of the first character in the string.
--SW(0) actually represents whether or not it is reading the 1st or 2nd character at the address.
--SW(13 downto 1) represents the address. 
--KEY(3) ? Adds the current string as an input. If less than 8 characters are to be read,
--then spaces fill the remaining space. Up to 6 inputs can be added.
--KEY(2) ? Adds the current string as an output. If less than 8 characters are to be read,
--then spaces fill the remaining space. Up to 6 inputs can be added.
--
--Output (Non VGA):
--
--This is used for debugging purposes. The on board results don?t mean anything.
--
--LEDR(17) ? Indicates that the text is done being read from SRAM into VGA. Asserts for only one clock tick.
--LEDR(16) ? Indicates that the text is currently being read from SRAM into VGA
--LEDR(11 downto 0), LEDG ? The full address that is currently being read from SRAM



  library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.STD_LOGIC_UNSIGNED.ALL;
  use IEEE.STD_LOGIC_ARITH.ALL;
  use ieee.numeric_std.all;
  use IEEE.vga_package.all;
  USE  IEEE.sram_package.all;
  
  entity vga_sram_at_location is
    port(CLOCK_50  : in std_logic;
	      SW  : in std_logic_vector(17 downto 0);
			KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			LEDR: out std_logic_vector(17 downto 0);
			LEDG: out std_logic_vector(7 downto 0);
			
			-- SRAM
			SRAM_ADDR: out std_logic_vector(19 downto 0);
			SRAM_DQ: inout std_logic_vector(15 downto 0);
			SRAM_CE_N: out std_logic;
			SRAM_OE_N: out std_logic;
			SRAM_WE_N: out std_logic;
			SRAM_UB_N: out std_logic;
			SRAM_LB_N: out std_logic;
			
			-- VGA
			VGA_R   : out std_logic_vector(7 downto 0);
			VGA_G : out std_logic_vector(7 downto 0);
         VGA_B  : out std_logic_vector(7 downto 0);
         VGA_HS    : out std_logic;
         VGA_VS    : out std_logic;
			VGA_SYNC_N      : out std_logic;
			VGA_BLANK_N     : out std_logic;
			VGA_CLK	 : out std_logic);
  end vga_sram_at_location;
 
  architecture Behavioral_vga_sram_at_location of vga_sram_at_location is
		COMPONENT vga2 
		PORT(
				CLOCK_50  : in std_logic;
				--write inputs
				reset  : in std_logic; -- reset on 1
				new_character  : in character;
				new_character_letter_num : in integer range letter_num_min to letter_num_max;
				new_character_io_num : in integer range 1 to 6;
				new_io : in std_logic; -- 1 is output, 0 to input
				new_write : in std_logic; -- only write on 1
				--VGA
				VGA_R   : out std_logic_vector(7 downto 0);
				VGA_G : out std_logic_vector(7 downto 0);
				VGA_B  : out std_logic_vector(7 downto 0);
				VGA_HS    : out std_logic;
				VGA_VS    : out std_logic;
				VGA_SYNC_N      : out std_logic;
				VGA_BLANK_N     : out std_logic;
				VGA_CLK	 : out std_logic
		);
		END COMPONENT;
	 
	 
		COMPONENT sram
		PORT(
			CLOCK_50  : in std_logic;
			new_write: in sram_write_array;
			new_address: in sram_address_type_array;
			new_input_data: in sram_data_type_array;
			new_enable: IN sram_enable_array;
			new_top_enable: IN sram_enable_array;
			new_bottom_enable: IN sram_enable_array;
			new_output_data: OUT sram_data_type_array;
			
			
			-- SRAM
			
			SRAM_ADDR: out std_logic_vector(19 downto 0);
			SRAM_DQ: inout std_logic_vector(15 downto 0);
			SRAM_CE_N: out std_logic;
			SRAM_OE_N: out std_logic;
			SRAM_WE_N: out std_logic;
			SRAM_UB_N: out std_logic;
			SRAM_LB_N: out std_logic
		);
		END COMPONENT;
	 
		COMPONENT clk_div 
		PORT
		(
			clock_50Mhz			: IN	STD_LOGIC;
			clock_1MHz				: OUT	STD_LOGIC;
			clock_100KHz			: OUT	STD_LOGIC;
			clock_10KHz				: OUT	STD_LOGIC;
			clock_1KHz				: OUT	STD_LOGIC;
			clock_100Hz				: OUT	STD_LOGIC;
			clock_10Hz				: OUT	STD_LOGIC;
			clock_1Hz				: OUT	STD_LOGIC
		);
		END COMPONENT;
		
	  signal clock_10Hz : STD_LOGIC;
	  signal reset : STD_LOGIC;
	  
	  signal new_character  : character;
	  signal new_character_letter_num : integer range letter_num_min to letter_num_max;
	  signal new_character_io_num : integer range 1 to 6;
	  signal new_io : std_logic; -- 1 is output, 0 to input
	  signal new_write : std_logic; -- only write on 1
	  
	  signal start_address : std_logic_vector(20 downto 0);
	  signal io_length : integer range 0 to letter_num_max;
	  signal input_count : integer range 0 to 6;
	  signal output_count : integer range 0 to 6;
	  
	  signal start_address_read : std_logic;
	  signal done_address_read : std_logic;
	  
	  
	  signal new_sram_input_data: sram_data_type_array;
	  signal new_sram_output_data: sram_data_type_array;
	  signal new_sram_address: sram_address_type_array;
	  signal new_sram_write: sram_write_array;
	  signal new_sram_enable: sram_enable_array;
	  signal new_sram_top_enable: sram_enable_array;
	  signal new_sram_bottom_enable: sram_enable_array;
	  
	  signal start_input_write : std_logic;
	  signal start_output_write : std_logic;
	  signal start_write : std_logic;
  begin
  
  LEDR(17) <= done_address_read;
  LEDR(16) <= start_address_read;
  
  
	reset <= SW(17);
	io_length <= conv_integer(SW(16 downto 14)) + 1;
	start_address(20 downto 14) <= (others => '0');
	start_address(13 downto 0) <= SW(13 downto 0);
	
	start_input_write <= not KEY(3);
	start_output_write <= not KEY(2);
	
	start_write <= start_input_write or start_output_write;
  process (reset, start_write, done_address_read, clock_10Hz)
	  variable v_start_address_read : std_logic;
	  variable v_new_io : std_logic;
  begin
		if (reset='1') then
			new_io <= '0';
			start_address_read <= '0';
			input_count <= 1;
			output_count <= 1;
		elsif clock_10Hz'event and clock_10Hz='1' then
			if (done_address_read='1' and start_address_read='1') then
				new_io <= new_io;
				start_address_read <= '0';
				if (new_io = '0') then
					input_count <= input_count + 1;
				else
					output_count <= output_count + 1;
				end if;
			elsif start_input_write='1' and start_address_read='0' then
				new_io <= '0';
				start_address_read <= '1';
			elsif start_output_write='1' and start_address_read='0' then
				new_io <= '1';
				start_address_read <= '1';
			elsif start_address_read='0' then
				new_io <= '0';
				start_address_read <= '0';
			else
				new_io <= new_io;
				start_address_read <= start_address_read;
			end if;
		end if;
  end process;
	
	
	
	new_sram_write(0) <= '0';
	new_sram_input_data(0)(15 downto 0) <= (others => '0');
	new_sram_enable(0) <= '1';
	new_sram_top_enable(0) <= '1';
	new_sram_bottom_enable(0) <= '1';
  
  
	sram1: sram	PORT MAP(
		CLOCK_50 => CLOCK_50,
		
		new_write => new_sram_write,
		new_address => new_sram_address,
		new_input_data => new_sram_input_data,
		new_enable => new_sram_enable,
		new_top_enable => new_sram_top_enable,
		new_bottom_enable => new_sram_bottom_enable,
		new_output_data => new_sram_output_data,
		SRAM_ADDR => SRAM_ADDR,
		SRAM_DQ => SRAM_DQ,
		SRAM_CE_N => SRAM_CE_N,
		SRAM_OE_N => SRAM_OE_N,
		SRAM_WE_N => SRAM_WE_N,
		SRAM_UB_N => SRAM_UB_N,
		SRAM_LB_N => SRAM_LB_N
	 ); --address
  
  
	clk_div1 : clk_div port map
	(	
	  clock_50Mhz  => CLOCK_50,
	  clock_10Hz => clock_10Hz
	);
	  
  process (reset,start_address_read,clock_10Hz)
	  variable step : integer range -1 to letter_num_range * 2;
	  variable character_number : integer range 0 to 2**21 - 1;
	  variable address_number : integer range 0 to 2**20 - 1;
	  variable high_low : integer range 0 to 1;
	  variable new_input_number : integer range 0 to 6;
	  variable new_io_letter : integer range 0 to letter_num_max;
	  variable new_address : std_logic_vector(19 downto 0);
  begin
		new_io_letter := ((step / 2) mod (letter_num_range));
		character_number := conv_integer(start_address) + new_io_letter;
		high_low := (character_number mod 2);
		address_number := character_number / 2;
		new_address(19 downto 0) := conv_std_logic_vector(address_number, 20);
    if start_address_read='0' then
		done_address_read <= '0';
		step := 0;
		new_write <= '0';
    elsif clock_10Hz'event and clock_10Hz='1' then
		if (step >= letter_num_range * 2) then
			done_address_read <= '1';
			new_write <= '0';
		else
			if (step MOD 2 = 0) then
				new_sram_address(0) <= new_address;
				LEDR(11 downto 0) <= new_address(19 downto 8);
				LEDG <= new_address(7 downto 0);
				new_write <= '0';
			elsif (step MOD 2 = 1) then
				new_write <= '1';
				
				if (new_io_letter >= io_length) then
					new_character <= ' ';
				elsif (high_low = 0) then
					new_character <= character'val(conv_integer(new_sram_output_data(0)(7 downto 0)));
				elsif (high_low = 1) then
					new_character <= character'val(conv_integer(new_sram_output_data(0)(15 downto 8)));
				end if;
				
				new_character_letter_num <= new_io_letter + letter_num_min;
				
				if (new_io = '1') then
					new_character_io_num <= output_count;
				elsif (new_io = '0') then
					new_character_io_num <= input_count;
				end if;
			end if;
			step := step + 1;
      end if;
    end if;
  end process;
	  
	  
  
      vga2X : vga2 port map
			(	
			  CLOCK_50  => CLOCK_50,
			  reset => reset,
			  new_character => new_character,
			  new_character_letter_num => new_character_letter_num,
			  new_character_io_num => new_character_io_num,
			  new_io => new_io,
			  new_write => new_write,
			  
			  
			  VGA_R => VGA_R,
			  VGA_G => VGA_G,
			  VGA_B => VGA_B,
			  VGA_HS => VGA_HS,
			  VGA_VS => VGA_VS,
			  VGA_SYNC_N => VGA_SYNC_N,
			  VGA_BLANK_N => VGA_BLANK_N,
			  VGA_CLK => VGA_CLK
		   );
			
			
  
  end Behavioral_vga_sram_at_location;