--Thomas Bruestle
--Greg Sleasman
--
--Sram_package.vhd
--
--These are the constants and types used in SRAM.

library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

package sram_package is

	constant sram_item_min : integer := 0;
	constant sram_item_max : integer := 0;
	
	subtype sram_address_type is std_logic_vector(19 downto 0);
	subtype sram_data_type is std_logic_vector(15 downto 0);
	
	
	type  sram_address_type_array  is array (sram_item_min to sram_item_max) of sram_address_type;
	type  sram_data_type_array  is array (sram_item_min to sram_item_max) of sram_data_type;
	type  sram_write_array  is array (sram_item_min to sram_item_max) of std_logic;
	type  sram_enable_array  is array (sram_item_min to sram_item_max) of std_logic;
	
end sram_package;