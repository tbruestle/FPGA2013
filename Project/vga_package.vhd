--Thomas Bruestle
--Greg Sleasman
--
--Vga_package.vhd
--
--These are the constants and types used in VGA.

library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

package vga_package is

	CONSTANT letter_num_min : integer := 1;
	CONSTANT letter_num_max : integer := 8;
	CONSTANT letter_num_range : integer := 8;
	
	
	
	CONSTANT letter_top : integer := 0;
	CONSTANT letter_bottom : integer := 9;
	CONSTANT letter_left : integer := 0;
	CONSTANT letter_right : integer := 7;
	
	CONSTANT input_num_min : integer := 1;
	CONSTANT input_num_max : integer := 6;
	
	CONSTANT output_num_min : integer := 1;
	CONSTANT output_num_max : integer := 6;
	
	
	CONSTANT screen_top : integer := 0;
	CONSTANT screen_bottom : integer := 524;
	
	
	CONSTANT screen_left : integer := 0;
	CONSTANT screen_right : integer := 799;
	

	
	
	type EIGHT_STRING is array(letter_num_min to letter_num_max) of Character;
	type ARRAY_OF_STRINGS is array (integer range <>) of EIGHT_STRING;
	
	type LETTER_BLOCK  is array (letter_top to letter_bottom) of std_logic_vector(letter_left to letter_right);
	type LETTER_BLOCK_ARRAY is array (integer range <>) of LETTER_BLOCK;
	type ARRAY_OF_BLOCKS is array (integer range <>) of LETTER_BLOCK_ARRAY(letter_num_min to letter_num_max);

  CONSTANT inleft : integer := 238; --The leftmost point of the leftmost character of inputs
  CONSTANT outleft : integer := 576; --The leftmost point of the leftmost character of outputs
  CONSTANT letters_top : integer := 152; -- The top of the first inputs/outputs
  CONSTANT letter_width_between : integer := 12; -- The distance between the leftmost of char1 and char2...
  CONSTANT letter_height_between : integer := 33; -- The distance between the topmost of input1 and input2...
end vga_package;