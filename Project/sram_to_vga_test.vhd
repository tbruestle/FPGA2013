--Thomas Bruestle
--Greg Sleasman
--
--Sram_to_vga_test.vhd (Top level entity) 
--
--This will write the first 96 characters loaded into SRAM into VGA.
--The characters are written sequentially, filling up all the inputs then all the outputs.
--This is used to test the concept of loading into VGA from SRAM. 


  library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.STD_LOGIC_UNSIGNED.ALL;
  use IEEE.STD_LOGIC_ARITH.ALL;
  use ieee.numeric_std.all;
  use IEEE.vga_package.all;
  use IEEE.sram_package.all;
 
	
  entity sram_to_vga_test is
    port(CLOCK_50  : in std_logic;
	      SW  : in std_logic_vector(17 downto 0);
			KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			LEDR: out std_logic_vector(17 downto 0);
			LEDG: out std_logic_vector(7 downto 0);
			
			
			-- SRAM
			
			SRAM_ADDR: out std_logic_vector(19 downto 0);
			SRAM_DQ: inout std_logic_vector(15 downto 0);
			SRAM_CE_N: out std_logic;
			SRAM_OE_N: out std_logic;
			SRAM_WE_N: out std_logic;
			SRAM_UB_N: out std_logic;
			SRAM_LB_N: out std_logic;
			
			-- VGA
			VGA_R   : out std_logic_vector(7 downto 0);
			VGA_G : out std_logic_vector(7 downto 0);
         VGA_B  : out std_logic_vector(7 downto 0);
         VGA_HS    : out std_logic;
         VGA_VS    : out std_logic;
			VGA_SYNC_N      : out std_logic;
			VGA_BLANK_N     : out std_logic;
			VGA_CLK	 : out std_logic);
  end sram_to_vga_test;
 
  architecture Behavioral_sram_to_vga_test of sram_to_vga_test is
  
  COMPONENT vga2_wrapper
  PORT(	CLOCK_50  : in std_logic;
	      new_input  : in ARRAY_OF_STRINGS (1 to 6);
	      new_output: in ARRAY_OF_STRINGS (1 to 6);
	      SW  : in std_logic_vector(17 downto 0);
			VGA_R   : out std_logic_vector(7 downto 0);
			VGA_G : out std_logic_vector(7 downto 0);
         VGA_B  : out std_logic_vector(7 downto 0);
         VGA_HS    : out std_logic;
         VGA_VS    : out std_logic;
			VGA_SYNC_N      : out std_logic;
			VGA_BLANK_N     : out std_logic;
			VGA_CLK	 : out std_logic);
	END COMPONENT;
	
	
	COMPONENT sram
	PORT(
		CLOCK_50  : in std_logic;
		new_write: in sram_write_array;
		new_address: in sram_address_type_array;
		new_input_data: in sram_data_type_array;
		new_enable: IN sram_enable_array;
		new_top_enable: IN sram_enable_array;
		new_bottom_enable: IN sram_enable_array;
		new_output_data: OUT sram_data_type_array;
		
		
		-- SRAM
		
		SRAM_ADDR: out std_logic_vector(19 downto 0);
		SRAM_DQ: inout std_logic_vector(15 downto 0);
		SRAM_CE_N: out std_logic;
		SRAM_OE_N: out std_logic;
		SRAM_WE_N: out std_logic;
		SRAM_UB_N: out std_logic;
		SRAM_LB_N: out std_logic
	);
	END COMPONENT; 
	
	COMPONENT clk_div 
	PORT
	(
		clock_50Mhz			: IN	STD_LOGIC;
		clock_1MHz				: OUT	STD_LOGIC;
		clock_100KHz			: OUT	STD_LOGIC;
		clock_10KHz				: OUT	STD_LOGIC;
		clock_1KHz				: OUT	STD_LOGIC;
		clock_100Hz				: OUT	STD_LOGIC;
		clock_10Hz				: OUT	STD_LOGIC;
		clock_1Hz				: OUT	STD_LOGIC
	);
	END COMPONENT;

	  signal input_count, output_count : std_logic_vector(2 downto 0);
	  signal new_input : ARRAY_OF_STRINGS (1 to 6);
	  signal new_output : ARRAY_OF_STRINGS (1 to 6);
	  signal input : ARRAY_OF_STRINGS (1 to 6);
	  signal output : ARRAY_OF_STRINGS (1 to 6);
	  signal clock_10Hz : STD_LOGIC;
	  signal clock_1Hz : STD_LOGIC;
	  
	  signal new_character  : character;
	  signal new_character_letter_num : integer range letter_num_min to letter_num_max;
	  signal new_character_io_num : integer range 1 to 6;
	  signal new_io : std_logic; -- 1 is output, 0 to input
	  signal new_write : std_logic; -- only write on 1
	  
	  
		signal new_sram_input_data: sram_data_type_array;
		signal new_sram_output_data: sram_data_type_array;
		signal new_sram_address: sram_address_type_array;
		signal new_sram_write: sram_write_array;
		signal new_sram_enable: sram_enable_array;
		signal new_sram_top_enable: sram_enable_array;
		signal new_sram_bottom_enable: sram_enable_array;
  begin
  
	new_sram_input_data(0)(15 downto 0) <= (others => '0');

	new_sram_write(0) <= '0';
	
	new_sram_top_enable(0) <= '1';
	new_sram_bottom_enable(0) <= '1';
	new_sram_enable(0) <= '1';
	
  
  process (clock_10Hz)
	  variable step : integer;
	  variable address_number : integer range 0 to 6 * letter_num_range * 2;
	  variable new_io_number : integer range 0 to 12;
	  variable new_io_letter : integer range 0 to letter_num_max;
	  variable new_address : std_logic_vector(19 downto 0);
  begin
		new_io_number := (step / letter_num_range);
		new_io_letter := (step mod letter_num_range);
		address_number := ((step / 2) mod (6 * letter_num_range * 2));
		new_address(19 downto 7) := (others => '0');
		new_address(6 downto 0) := conv_std_logic_vector(address_number, 7);
    if clock_10Hz'event and clock_10Hz='1' then
      if (new_io_number >= 12) then
			step := 0;
		else
			if (step MOD 2 = 0) then
				new_sram_address(0) <= new_address;
				LEDR(11 downto 0) <= new_address(19 downto 8);
				LEDG <= new_address(7 downto 0);
			elsif (step MOD 2 = 1) then
				if (new_io_number < 6) then
					new_input(new_io_number + 1)(new_io_letter + letter_num_min - 1) <= character'val(conv_integer(new_sram_output_data(0)(7 downto 0)));
					new_input(new_io_number + 1)(new_io_letter + letter_num_min) <= character'val(conv_integer(new_sram_output_data(0)(15 downto 8)));
				else				
					new_output(new_io_number - 5)(new_io_letter + letter_num_min - 1) <= character'val(conv_integer(new_sram_output_data(0)(7 downto 0)));
					new_output(new_io_number - 5)(new_io_letter + letter_num_min) <= character'val(conv_integer(new_sram_output_data(0)(15 downto 8)));
				end if;
			end if;
			step := step + 1;
      end if;
    end if;
  end process;
  
  
  
  
  
		clk_div1 : clk_div port map
		(	
		  clock_50Mhz  => CLOCK_50,
		  clock_10Hz => clock_10Hz,
		  clock_1Hz => clock_1Hz
		);
  
  
      vga2_wrapperX : vga2_wrapper port map
			(	
			  CLOCK_50  => CLOCK_50,
			  new_input => new_input,
			  new_output => new_output,
			  SW => SW,
			  VGA_R => VGA_R,
			  VGA_G => VGA_G,
			  VGA_B => VGA_B,
			  VGA_HS => VGA_HS,
			  VGA_VS => VGA_VS,
			  VGA_SYNC_N => VGA_SYNC_N,
			  VGA_BLANK_N => VGA_BLANK_N,
			  VGA_CLK => VGA_CLK
		   );
			
			
		sram1: sram	PORT MAP(
			CLOCK_50 => CLOCK_50,
			new_write => new_sram_write,
			new_address => new_sram_address,
			new_input_data => new_sram_input_data,
			new_enable => new_sram_enable,
			new_top_enable => new_sram_top_enable,
			new_bottom_enable => new_sram_bottom_enable,
			new_output_data => new_sram_output_data,
			SRAM_ADDR => SRAM_ADDR,
			SRAM_DQ => SRAM_DQ,
			SRAM_CE_N => SRAM_CE_N,
			SRAM_OE_N => SRAM_OE_N,
			SRAM_WE_N => SRAM_WE_N,
			SRAM_UB_N => SRAM_UB_N,
			SRAM_LB_N => SRAM_LB_N
		 ); --address
  
  end Behavioral_sram_to_vga_test;