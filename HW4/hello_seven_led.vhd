-- seven_led.vhd
--
-- Function: convert a 4-bit binary input into a HEX number

library ieee;                                   
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

Entity hello_seven_led is
	port(	char_in:	in character;
			hex_out:	out bit_vector (0 to 6));
end hello_seven_led;

architecture beha of hello_seven_led is
begin
	with char_in select
		hex_out<="1111111" when ' ',
					"1001000" when 'H',
					"0110000" when 'E',
					"1110001" when 'L',
					"0000001" when 'O',
					"1111111" when others;
end beha;
