-- Thomas Bruestle
--	HW 4

--Assignment:

--Design a circuit using VHDL for the DE2-115 board that scrolls the word \HELLO" in
--ticker-tape fashion on the eight 7-segment displays. The letters should move from right to
--left each time you apply a manual clock pulse to the circuit. After the word \HELLO"
--scrolls off the left side of the displays it then starts again on the right side. Design your
--circuit by using eight 7-bit registers connected in a queue-like fashion, such that the outputs
--of the first register feed the inputs of the second, the second feeds the third, and so on. This
--type of connection between registers is often called a pipeline. Each registers outputs should
--directly drive the seven segments of one display.

--You are to design a finite state machine that controls the pipeline in two ways:
--1. For the first eight clock pulses after the system is reset, the FSM inserts the correct
--characters (H,E,L,L,O, , , ) into the first of the 7-bit registers in the pipeline.
--2. After Step 1 is complete, the FSM configures the pipeline into a loop that connects
--the last register back to the first one, so that the letters continue to scroll indefinitely.
--Write VHDL code for the ticker-tape circuit and create a Quartus II project for your
--design. Use KEY0 on the DE2-115 board to clock the FSM and pipeline registers and use
--SW0 as a synchronous active-low reset input. Compile your VHDL code, download it onto
--the DE2-115 board and test the circuit.

library ieee;                                   
use ieee.std_logic_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;


entity HW4 is
port(
	SW : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	KEY : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	HEX0 : out bit_vector (0 to 6);
	HEX1 : out bit_vector (0 to 6);
	HEX2 : out bit_vector (0 to 6);
	HEX3 : out bit_vector (0 to 6);
	HEX4 : out bit_vector (0 to 6);
	HEX5 : out bit_vector (0 to 6);
	HEX6 : out bit_vector (0 to 6);
	HEX7 : out bit_vector (0 to 6)
);
end HW4;

architecture struc1 of HW4 is

	type CHAR_ARRAY is array (integer range <>) of character;
	signal chars: CHAR_ARRAY(0 to 7);
	signal state: integer range 1 to 9;
	
--	
	COMPONENT hello_seven_led
	PORT(	
		char_in:	in character;
		hex_out:	out bit_vector (0 to 6)
		);
	END COMPONENT; 

begin


		
	process(sw,  KEY) 
	begin 
		if sw(0)='0' then --reset
			chars <= "        ";
			state <= 1;
		elsif KEY'event and KEY(0)='1' then
			if state < 4 then
			   state <= state + 1;			
			elsif state = 4 then
			   chars(0) <= 'H';
			   state <= state + 1;
			elsif state = 5 then
			   chars(0) <= 'E';
			   state <= state + 1;
			elsif state = 6 then
			   chars(0) <= 'L';
			   state <= state + 1;
			elsif state = 7 then
			   chars(0) <= 'L';
			   state <= state + 1;
			elsif state = 8 then
			   chars(0) <= 'O';
			   state <= state + 1;
			elsif state = 9 then
			   chars(0) <= chars(7);
			end if; 
			
			chars(1) <= chars(0);
			chars(2) <= chars(1);
			chars(3) <= chars(2);
			chars(4) <= chars(3);
			chars(5) <= chars(4);
			chars(6) <= chars(5);
			chars(7) <= chars(6);
		end if; 
	end process; 
	
	sled0: hello_seven_led	PORT MAP( char_in => chars(0), hex_out(0 to 6) => HEX0(0 to 6) );
	sled1: hello_seven_led	PORT MAP( char_in => chars(1), hex_out(0 to 6) => HEX1(0 to 6) );
	sled2: hello_seven_led	PORT MAP( char_in => chars(2), hex_out(0 to 6) => HEX2(0 to 6) );
	sled3: hello_seven_led	PORT MAP( char_in => chars(3), hex_out(0 to 6) => HEX3(0 to 6) );
	sled4: hello_seven_led	PORT MAP( char_in => chars(4), hex_out(0 to 6) => HEX4(0 to 6) );
	sled5: hello_seven_led	PORT MAP( char_in => chars(5), hex_out(0 to 6) => HEX5(0 to 6) );
	sled6: hello_seven_led	PORT MAP( char_in => chars(6), hex_out(0 to 6) => HEX6(0 to 6) );
	sled7: hello_seven_led	PORT MAP( char_in => chars(7), hex_out(0 to 6) => HEX7(0 to 6) );
	
end struc1;