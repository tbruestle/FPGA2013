library ieee;
use ieee.std_logic_1164.all;

entity top_level_entity is
	port(
		SW: in std_logic_vector(17 downto 0);
		LEDR: out std_logic_vector(17 downto 0);
		
		SRAM_ADDR: out std_logic_vector(19 downto 0);
		SRAM_DQ: inout std_logic_vector(15 downto 0);
		SRAM_CE_N: out std_logic;
		SRAM_OE_N: out std_logic;
		SRAM_WE_N: out std_logic;
		SRAM_UB_N: out std_logic;
		SRAM_LB_N: out std_logic
	);
end top_level_entity;


architecture inside_top_level_entity of top_level_entity is

	signal address: std_logic_vector(1 downto 0);
	signal data: std_logic_vector(7 downto 0);
	signal output: std_logic_vector(7 downto 0);
	signal we: std_logic;
	
begin

	address <= SW(17 downto 16);
	data <= SW(7 downto 0) when SW(15) = '1' else (others => 'Z');
	LEDR(7 downto 0) <= output;

	SRAM_WE_N <= not SW(15);
	
	SRAM_CE_N <= '0';
	SRAM_OE_N <= '0';
	SRAM_UB_N <= '0';
	SRAM_LB_N <= '0';
	
	SRAM_ADDR(19 downto 2) <= (others => '0');
	SRAM_ADDR(1 downto 0) <= address;
	
	SRAM_DQ(15 downto 8) <= (others => '0');
	SRAM_DQ(7 downto 0) <= data;
	
	output <= SRAM_DQ(7 downto 0);

end inside_top_level_entity;